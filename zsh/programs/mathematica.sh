function _fixMathematicaInstallation () {
    install_foolder="/usr/local/Wolfram/Mathematica/11.3/SystemFiles/Libraries/Linux-x86-64"
    cd ${install_foolder}

    sudo mv libfreetype.so.6 libfreetype.so.6.bak
    sudo mv libz.so.1 libz.so.1.bak
}
alias fixMathematicaInstallation='_fixMathematicaInstallation'
