import dbus
import argparse


def get_args():
    parser = argparse.ArgumentParser(usage='-m <manage DBUS MediaPlayer>')

    parser.add_argument(
        '-m',
        dest='manage',
        help='[ pp -> toggle Play/Pause, n -> next, prev -> previous ]',
        default='pp',
        type=str,
        required=True
    )

    return parser.parse_args()


def main(args):
    bus = dbus.SessionBus()

    player_service = 'org.mpris.MediaPlayer2.Player'
    is_player = lambda x: x.startswith('org.mpris.MediaPlayer2.')
    dbus_properties = 'org.freedesktop.DBus.Properties'

    for service in bus.list_names():
        if is_player(service):
            player = bus.get_object(service, '/org/mpris/MediaPlayer2')

            status = player.Get(player_service, 'PlaybackStatus', dbus_interface=dbus_properties)

            if status != 'Stopped':
                interface = dbus.Interface(player, dbus_interface=player_service)

                mode = args['manage']
                if mode == 'pp':
                    interface.PlayPause()
                elif mode == 'n':
                    interface.Next()
                elif mode == 'prev':
                    interface.Previous()

                # metadata = player.Get(player_service, 'Metadata', dbus_interface=dbus_properties)


if __name__ == '__main__':
    args = get_args()
    main(vars(args))
