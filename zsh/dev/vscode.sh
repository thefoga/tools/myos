function _vsCodeInstall () {
  name="${1}"
  code --install-extension ${name} --force
}
alias vsCodeInstall='_vsCodeInstall'
