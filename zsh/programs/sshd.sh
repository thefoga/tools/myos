export SSHD_LOG='/var/log/auth.log'

alias showSSHDLogin='ag "accepted" ${SSHD_LOG}'

function probePortHost () {
  port=$1
  host=$2

  cmd="telnet ${host} ${port}"
  eval ${cmd}
}
alias testSSHOpen='probePortHost 22'
