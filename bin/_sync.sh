source "../_helpers/links.sh"


THIS_FOLDER="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
BIN_FOLDER=${HOME}/.local/bin

# Symbolic link of . files in ~bin folder from this folder
function here2BinFile {
  dotFile=$1
  fullDotFilePath=${THIS_FOLDER}/${dotFile}
  dest=${BIN_FOLDER}/$1

  linkFile ${fullDotFilePath} ${dest}
  chmod +x ${dest}
}

# here2BinFile ipstack.sh
# here2BinFile dd.py

mkdir -p ${HOME}/opt/jupiterhub
linkFileFromHere jupyterNotebook.sh ${HOME}/opt/jupyterhub/jupyterNotebook.sh
