# Downloads everything from FTP in HOST with PASS
# usage: downloadFromFPT <USER> <HOST> <PASS>
function _downloadFromFPT () {
    user=$(echo $1 | sed 's/"//g')  # remove "
    host=$(echo $2 | sed 's/"//g')  # remove "
    password=$(echo $3 | sed 's/"//g')  # remove "

    lftp -c "open -u ${user},${password} ${host}; mirror --parallel=10"
}
alias downloadFromFPT='_downloadFromFPT'

# Gets FTP password of user in domain
# Usage: getFTPPassword <domain> <user>
# Example: getFTPPassword '."me"."home"."rasp"' 'pi'
# Depends on: getPassword
function getFTPPassword () {
    domain=$1
    user=$2
    key='.${domain}.${user}."pass"'
    cmd="getJsonVal ${key} ${FTP_CREDENTIALS}"
    val=$(eval ${cmd})
    echo ${val}
}

# Gets FTP host of domain
# Usage: getFTPHost <domain>
# Example: getFTPHost '."me"."home"."rasp"'
# Depends on: getHost
function getFTPHost () {
    domain=$1
    cmd="getHost ${domain} ${FTP_CREDENTIALS}"
    val=$(eval ${cmd})
    echo ${val}
}

USER_THEFOGA='"foga"'
DOMAIN_THEFOGA="thefoga"
PASS_THEFOGA=$(getFTPPassword ${DOMAIN_THEFOGA} ${USER_THEFOGA})
HOST_THEFOGA=$(getFTPHost ${DOMAIN_THEFOGA})

alias downloadMyFTP='downloadFromFPT ${USER_THEFOGA} ${HOST_THEFOGA} ${PASS_THEFOGA}'
