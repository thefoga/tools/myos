function _flv2gif () {
  inFile=$1
  outFile=${inFile}.gif
  ffmpeg -i ${inFile} -vf scale=320:-1 -r 10 -f image2pipe -vcodec ppm - | convert -delay 5 -loop 0 - ${outFile}
}
alias flv2gif='_flv2gif'

# `cat *.vob > ~/Desktop/output.vob` from DVD
# https://www.internalpointers.com/post/convert-vob-files-mkv-ffmpeg
function _vob2mkv () {
  inFile=$1
  outFile=${inFile}.mkv

  ffmpeg \
    -analyzeduration 100M -probesize 100M \
    -i ${inFile} \
    -codec:v libx264 -crf 21 \
    -codec:a libmp3lame -qscale:a 2 \
    -codec:s copy \
    ${outFile}
}

alias vob2mkv='_vob2mkv'
