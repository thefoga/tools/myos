VERSION=0.9.0
CODENAME=linux-x86_64
FILE_OUT=exa.zip
INSTALL_FOLDER=${HOME}/.local/bin/

wget https://github.com/ogham/exa/releases/download/v${VERSION}/exa-${CODENAME}-${VERSION}.zip -O ${FILE_OUT}
unzip ${FILE_OUT}

mkdir -p ${INSTALL_FOLDER}
mv exa-linux-x86_64 ${INSTALL_FOLDER}
rm ${FILE_OUT}
