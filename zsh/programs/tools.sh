PASS_LEN=32

function doAndEcho () {
    val=$(eval $1)
    echo ${val}
}

# rules:
# - A: alpha
# - N: numbers

# all possible combo of [A, N]
alias  genAPass='doAndEcho "cat /dev/urandom | tr -dc 'a-zA-Z' | fold -w ${PASS_LEN} | head -n 1"'
alias  genNPass='doAndEcho "cat /dev/urandom | tr -dc '0-9' | fold -w ${PASS_LEN} | head -n 1"'
alias genANPass='doAndEcho "cat /dev/urandom | tr -dc 'a-zA-Z0-9' | fold -w ${PASS_LEN} | head -n 1"'

# Gets value of key in .json file
# Usage: getJsonVal <key> <json file>
# Example: getJsonVal '."a"."b"' 'file.json'
# Depends on: jq
function _getJsonVal () {
    key=$1
    filePath=$2
    cmd="jq '${key}' ${filePath}"
    val=$(eval ${cmd})
    echo ${val}
}
alias getJsonVal='_getJsonVal'

alias 2cb='xclip -selection clipboard'

alias cronLog='cat /var/log/cron.log'

function _getQR () {
  echo $1 | curl -F-=\<- qrenco.de  
}
alias getQR='_getQR'

alias editTODO='vim ${TODO_FILE}'
