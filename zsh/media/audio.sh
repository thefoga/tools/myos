# e.g playNoise "2" "1:00:00" "brown" "1200" "400" "tremolo 200 10" "2"
function playNoise () {
    channels=${1}
    length=${2}
    
    kind=${3}
    
    band=${4}
    width=${5}

    effects=${6}

    repeats=${7}

    cmd="play -c ${channels} -n synth ${length} ${kind}noise band -n ${band} ${width} ${effects} repeat ${repeats}"
    eval ${cmd}
}

alias makeNoise='playNoise "2" "1:00:00" "brown" "1200" "400" "tremolo 200 10" "2"'
alias makeNoiseOceanWaves='play -n synth brownnoise synth pinknoise mix synth sine amod 0.3 10'  # https://askubuntu.com/a/789472
alias makeNoiseTorture='pacat /dev/urandom'  # https://askubuntu.com/a/789499

