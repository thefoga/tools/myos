# !/bin/bash
# coding: utf-8


# project settings
PROJECT_NAME=""
PROJECT_WEBPAGE=""
PROJECT_AUTHOR="sirfoga"
PROJECT_AUTHOR_EMAIL="sirfoga@protonmail.ch"
PROJECT_DESCRIPTION=""
PROJECT_ARGS="-h help, -o <option>"

# script settings
CURRENT_FOLDER=$(pwd)

# help functions
function showHelp {
	echo $PROJECT_DESCRIPTION" Use as follows:"
    echo $PROJECT_ARGS
}

function errArgs {
    echo "Wrong or incorrect use of arguments."
    showHelp
}

function showDebugMessage() {
	echo
	echo "$1"
	echo
}

# check args
if [[ "$#" != 1 ]]; then
	errArgs
	exit 1
fi

# parse args
argsNumberOne=$1

# show debug
showDebugMessage "Done!"
