USER_AGENT="Mozilla/5.0 (Windows NT 6.3; rv:36.0) Gecko/20100101 Firefox/36.0"

function downloadTo () {
  url=$1
  targetFile=$2

  curl -A ${USER_AGENT} ${url} --output ${targetFile}
  exitCode=$?
  if [ ${exitCode} -eq 0 ]
  then
    echo "${url} ~> ${targetFile}"
  else
    echo "Could not download ${url} to ${targetFile} !!!"
  fi
}

function downloadHere () {
  url=$1
  fileName=$(basename ${url})
  targetFile=$(whereIam)/${fileName}

  downloadTo ${url} ${targetFile}
}

function getArxivID () {
  arxiv=$1  # can be anything (url, pdf, id)
  echo ${arxiv} | awk '{n=split($0, a, "/"); print a[n]}'  # id is last
}

function downloadArxivPDF () {
  arxiv=$1  # can be anything (url, pdf, id)
  id=$(getArxivID ${arxiv})
  url="https://arxiv.org/pdf/${id}.pdf"

  downloadHere ${url}
}

function downloadArxivTAR () {
  arxiv=$1  # can be anything (url, pdf, id)
  id=$(getArxivID ${arxiv})
  url="https://arxiv.org/e-print/${id}"

  outFile=${id}.tar.gz
  downloadTo ${url} ${outFile}

  outFolder=${id}
  mkdir ${outFolder}
  tar xvf "${outFile}" -C ${outFolder}
  rm ${outFile}
}

function getAllDocs () {
    url=$1
    curl ${1} | grep -Eo "(http|https)://[a-zA-Z0-9./?=_-]*.*(doc|docx|xls|xlsx|ppt|pptx|pdf)" | sort | uniq
}

alias apod='xdg-open https://apod.nasa.gov/apod/'

function _updateChromeDriver () {
  version=$1

  cd ~/opt/chromedriver_linux64
  wget https://chromedriver.storage.googleapis.com/${version}/chromedriver_linux64.zip
  unzip chromedriver_linux64.zip
  rm chromedriver_linux64.zip
}
alias updateChromeDriver=_updateChromeDriver
