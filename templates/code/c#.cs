using System;

/**
 * Namespace description
 */
namespace HelloWorld
{
    /**
     * Class description
     */
    class Hello 
    {
        /**
         * Method description
         */
        static void Main() 
        {
            Console.WriteLine("Hello World!");

            // Keep the console window open in debug mode.
            Console.WriteLine("Press any key to exit.");
            Console.ReadKey();
        }
    }
}
