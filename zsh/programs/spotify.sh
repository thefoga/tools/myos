# clears Spotify cache
alias clearSpotify='rm -rf ${HOME}/.cache/spotify/'
alias killSpotify="kill -9 $(ps -aux | ag '/spotify' | awk '{print $2}' | head -n 1)"
