alias apt='sudo apt'
alias aptI='sudo apt install -y'
alias aptR='sudo apt purge'
alias aptU='sudo apt update && sudo apt-get dist-upgrade && sudo apt-get autoremove'
alias aptFix='sudo rm -vf /var/lib/apt/lists/* && sudo apt update && sudo apt install -f'
alias aptSearchInstalled='apt list --installed | ag'
alias getRecentlyInstalledPackages='grep " install " /var/log/dpkg.log'
alias aptNotUpdate='sudo apt-mark hold'  # https://askubuntu.com/a/99780

alias aptRemoveKubuntu='sudo apt-get purge "^kde" "^kubuntu" "^plasma"'
