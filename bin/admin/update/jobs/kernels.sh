dpkg -l |  # list packages
  tail -n +6 |  # display meaningful details
  grep -E 'linux-image(-|-extra-)[0-9]+' |  # get only kernel-related packages ...
  grep -Fv $(uname -r) |  # ... except the one currently up and working
  awk '{print $2}' |  # get name of package only (and discard other info)
  xargs sudo apt purge -y  # actual remove (and purge)
