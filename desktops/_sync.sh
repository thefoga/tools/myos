source "../_helpers/links.sh"


THIS_FOLDER="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
APPS_FOLDER=${HOME}/.local/share/applications

# Symbolic link of . files in ~bin folder from this folder
function here2AppsFolder {
  dotFile=$1
  fullDotFilePath=${THIS_FOLDER}/${dotFile}
  dest=${APPS_FOLDER}/$1

  linkFile ${fullDotFilePath} ${dest}
  chmod +x ${dest}
}

#here2AppsFolder matlabR2020a.desktop
here2AppsFolder obsidian.desktop && here2AppsFolder obsidian.png
here2AppsFolder joplin.desktop && here2AppsFolder joplin.png
here2AppsFolder ledger.desktop && here2AppsFolder ledger.png
here2AppsFolder keepassxc.desktop && here2AppsFolder keepassxc.png
here2AppsFolder start-tor-browser.desktop
#here2AppsFolder sublime_merge.desktop
#here2AppsFolder turtl.desktop
here2AppsFolder zotero.desktop && here2AppsFolder zotero.png
