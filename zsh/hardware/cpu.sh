function setCPUOnOff () {
    cpu_num=$1
    state=$2

    cpu_file=/sys/devices/system/cpu/cpu${cpu_num}/online
    echo ${state} | sudo tee -a ${cpu_file}
}

function _bringCPUOnline () {
    cpu_num=$1
    setCPUOnOff ${cpu_num} 1
}
alias bringCPUOnline='_bringCPUOnline'

function _bringCPUOffline () {
    cpu_num=$1
    setCPUOnOff ${cpu_num} 0
}
alias bringCPUOffline='_bringCPUOffline'

alias showOnlineCPU='lscpu | ag "\-line"'
alias showCPUFanSpeed='sensors | grep -i fan'
