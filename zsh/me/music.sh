################ Fucking around

MUSIC_FOLDER=${HOME}/Music
MY_SOUNDS=${HOME}/.local/sounds

# plays porco dio
alias porcodio='printAndPlay "PORCO DIOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO" "${MY_SOUNDS}/porcodio.mp3"'

alias technoToujourPareill='printAndPlay "Techno\nToujours pareill\nBoum boum dans les oreilles\nMusique de défonce-man\nPas de message normal\nRien à dire" "${MY_SOUNDS}/ttp.mp3"'

alias revelations='printAndPlay "" "${MY_SOUNDS}/Revelations_Mystiques.mkv"'

# plays 'Dio merda - Cinese'
alias diomerda='printAndPlay "dio merda" "${MY_SOUNDS}/cinese/diomerda_cut.mp3"'

# plays pirates of carribean
alias pirate='printAndPlay "Piratesssssssssssssssss" "${MY_SOUNDS}/pirates.mp3"'

# plays Shokolade (Spongebob remix)
alias shokolade='printAndPlay "Schokolade!" "${MY_SOUNDS}/shokolade.mp3"'

infinityLyrics=$(cat ${MY_SOUNDS}/infinity.lyrics)
alias infinity='printAndPlay ${infinityLyrics} "${MY_SOUNDS}/infinity.mp3"'

# let's get ready to rumble
alias rumble='printAndPlay "Let''s get ready to rumble!" "${MY_SOUNDS}/rumble.mp3"'

# plays 'Prima di porco c'è dio - Valeria'
alias primaporco='printAndPlay "prima di porco cè dio" "${MY_SOUNDS}/valeria/prima_di_porco_dio.ogg"'

# plays '3 - Valeria'
alias tre='printAndPlay "3" "${MY_SOUNDS}/valeria/3.ogg"'

# plays 'Remember Remember'
alias remember='printAndPlay "Remember Remember the 5th of November" "${MUSIC_FOLDER}/Remember_Remember.mp3"'

alias nTime='printAndPlay "T.I.M.E. by Neil Cicierega" "${MUSIC_FOLDER}/Neil Cicierega/Neil Cicierega - T.I.M.E..mp3"'
alias mmhha='printAndPlay "Young Signorino Mmh Ha ha ha" "${MUSIC_FOLDER}/Young_Signorino_MmmhHaHaHa.mp3"'

# plays The Rolling Stones - Satisfaction
alias satisfaction='printAndPlay "" "${MUSIC_FOLDER}/Satisfaction.mp3"'

alias bonNadaeeeee='printAndPlay "BON NADAEEEEE" "${MY_SOUNDS}/bon_nadae.mp3"'
alias hakaaa='printAndPlay "" "${MY_SOUNDS}/haka.mkv"'
alias plantasia='printAndPlay "" "${MY_SOUNDS}/plantasia.mp3"'
alias good_morning='printAndPlay "don''t be so serious all the time :p" "${MY_SOUNDS}/good_morning.mp3"'
alias good_morning_vietnam='printAndPlay "gooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooood mornin Vietnam !" "${MY_SOUNDS}/good_morning_vietnam.mp3"'
