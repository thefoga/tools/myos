# History
# don't use duplicate lines or lines starting with space
HISTCONTROL=ignoreboth
HISTFILE=~/.zsh_history
HISTSIZE=10000
SAVEHIST=10000
setopt appendhistory

# search through terminal history
function _hs () {
  stuff=$1
  raw=$(ag --no-numbers ${stuff} ${HISTFILE})
  echo ${raw} | while read -r line; do
    dateTime=$(echo ${line} | awk -F':' '{ print $2 }' | sed 's/ //g');
    dateTime=$(date -d@${dateTime});
    contents=$(echo ${line} | awk -F':' '{ $1=""; $2=""; print $0 }' | awk -F';' '{ $1=""; print $0 }' | sed 's/^ //');
    echo -e ${TXT_GREEN}${dateTime}${COL_RESET}" ~> "${TXT_BLUE}${contents}${COL_RESET} > /dev/tty;
  done
}
alias hs='_hs'

# prints last exit code
alias e='echo $?'

alias lastCommand='fc -ln -1'
