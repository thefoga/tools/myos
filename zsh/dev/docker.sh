alias docker='sudo docker'

alias dockerStopContainers='docker kill $(docker ps -q)'
alias dockerRemoveContainers='docker rm $(docker ps -a -q)'
alias dockerRemoveImages='docker rmi $(docker images -q)'
alias dockerRemoveVolumes='docker volume ls -qf dangling=true | xargs -r docker volume rm'

alias dockerDisplay='sudo xhost +local:docker'
