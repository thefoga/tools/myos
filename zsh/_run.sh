aliasFiles=(
    ../_helpers/meta
    ../_helpers/links

    programs/defaults
    programs/tools
    programs/arduino
    programs/matlab
    programs/CA

    meta/colors
    meta/folders
    meta/history
    meta/root
    meta/credentials
    meta/aliases

    system/files
    system/folders
    system/net
    system/print

    admin/apt
    admin/processes
    admin/snap
    admin/sys
    admin/users
    admin/kde
    admin/deluge

    dev/vscode
    dev/scratch
    dev/docker
    dev/android
    dev/matlab
    dev/tools
    dev/ci
    dev/slurm
    dev/python

    media/images
    media/books
    media/csv
    media/pdf
    media/video
    media/audio
    media/mp3
    media/cam

    programs/defaults
    programs/git
    programs/medium
    programs/music
    programs/video
    programs/node
    programs/pdf
    programs/shortcuts
    programs/spotify
    programs/ftp
    programs/ssh
    programs/sshd
    programs/youtube
    programs/gdrive
    programs/virus
    programs/email
    programs/torch
    programs/garmin
    programs/mathematica

    tools/apache
    tools/services
    tools/crypto
    tools/user
    tools/web

    me/focus
    me/music
    me/sfw
    me/uibk

    hardware/asus
    hardware/bluetooth
    hardware/cpu
    hardware/battery
    hardware/android
    hardware/sound

    de/kde
    de/meta
)

for aliasName in "${aliasFiles[@]}"
do
    sourceAliasFile ${aliasName}
done
