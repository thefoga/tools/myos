alias ytAudio='youtube-dl --audio-format "mp3" --audio-quality 0 --extract-audio'
alias ytVideo='youtube-dl'

function ytFeedUrl () {
    ytid=$1
    echo "https://www.youtube.com/feeds/videos.xml?channel_id=${ytid}"
}

function ytFeed () {
    echo "get ID from https://commentpicker.com/youtube-channel-id.php"
    echo "... then \`ytFeedUrl ID\`"
}
