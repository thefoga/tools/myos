source ./meta/apt.sh

packages=(
  cups-backend-bjnp
  lm-sensors
  gir1.2-gtop-2.0  # https://github.com/paradoxxxzero/gnome-shell-system-monitor-applet#prerequisites
  gir1.2-nm-1.0
  gir1.2-clutter-1.0
  gnome-system-monitor
)

for package in "${packages[@]}"
do
  aptInstall ${package}
done
