alias svi='sudo vim'
alias super='sudo su'

# Red pointer for root
if [ "${UID}" -eq "0" ]; then
    pointerC="${txtred}"
fi
