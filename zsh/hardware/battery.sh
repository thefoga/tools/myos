_do_calc() { awk "BEGIN{ printf \"%.2f\n\", $* }"; }

function checkBatteryStats () {
    base2monitor=/sys/class/power_supply/BAT0

        keys=(
        capacity
        capacity_level
        cycle_count
        #energy_now
        status
        #voltage_now
        charge_control_end_threshold
    )

    for key in "${keys[@]}"
    do
        val=$(cat ${base2monitor}/${key})
        echo "${key} = ${val}"
    done

    energy_full=$(cat ${base2monitor}/energy_full)
    energy_full_design=$(cat ${base2monitor}/energy_full_design)
    energy_full_perc=$(_do_calc "${energy_full} / ${energy_full_design} * 100" | sed 's/,/./')
    echo "energy_full_perc = ${energy_full_perc} %"
}
