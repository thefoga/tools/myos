source ./meta/apt.sh

packages=(
  n2n
  nmap
  curl
  wireshark
  openvpn
)

for package in "${packages[@]}"
do
  aptInstall ${package}
done

eval $aliases[APT_UPDATE]
sudo dpkg-reconfigure wireshark-common
sudo adduser ${USER} wireshark

# bash <(curl -Ss https://my-netdata.io/kickstart.sh) all  # netdata
