_printAll () {
    extension=$1
    printer=$2

    PRINTER=${printer}
    lpstat -p -d

    lpoptions -d ${PRINTER}
    lpq
    for file in *.${extension}; do lpr -p ${file}; done
}

alias printAllPDFAtTeknik='_printAll "pdf" "[IFI_BW]_HP_LaserJet_M605dn_(PS)"'
