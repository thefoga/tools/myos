source ./meta/apt.sh
source ./meta/pip.sh

# apt
apt_packages=(
  # core
  python3-pip
  python3-dev
  build-essential

  # matplotlib
  python3-matplotlib
  libpng-dev
  libfreetype6-dev
  python3-tk

  # scipy
  python3-scipy
  python3-pandas
  python3-nose

  # networking
  python3-socks
)

for package in "${apt_packages[@]}"
do
  aptInstall ${package}
done

# pip
# pyhal
rFile=requirements.txt
wget -O ${rFile} https://raw.githubusercontent.com/sirfoga/pyhal/master/requirements.txt
pipInstall -r ${rFile}
rm ${rFile}  # clean

pip_packages=(
  thefuck

  scipy
  pandas
  nose
  
  ipython
  notebook
  
  sklearn
  wheel
  
  git+https://github.com/sirfoga/hal.git
  
  flake8
  scdl
  
  speedtest-cli
  google-api-python-client
  pipenv
  Sphinx
  
  torch
  torchvision

  jedi==0.17.2
)

for package in "${pip_packages[@]}"
do
  pipInstall ${package}
done

# torch (from https://pytorch.org/get-started/locally/)
pip3 install torch==1.8.1+cu111 torchvision==0.9.1+cu111 torchaudio==0.8.1 -f https://download.pytorch.org/whl/torch_stable.html
# just CPU: pip3 install torch==1.8.1+cpu torchvision==0.9.1+cpu torchaudio==0.8.1 -f https://download.pytorch.org/whl/torch_stable.html
