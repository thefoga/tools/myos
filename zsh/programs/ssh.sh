# logins USER in HOST with PASS
# usage: loginSSH <USER> <HOST> <PASS>
function _loginSSH () {
  user=$(echo $1 | sed 's/"//g')  # remove "
  host=$(echo $2 | sed 's/"//g')
  password=$3

  cmd="sshpass -p ${password} ssh -t ${user}@${host}"
  eval ${cmd}
}
alias loginSSH='_loginSSH'

# VPNC
alias connect-vpnc='sudo vpnc-connect'
alias disconnect-vpnc='sudo vpnc-disconnect'

# Gets SSH host of domain
# Usage: getSSHHost <domain>
# Example: getSSHHost '."me"."home"."rasp"'
# Depends on: getHost
function getSSHHost () {
    domain=$1
    cmd="getHost ${domain} ${SSH_CREDENTIALS}"
    val=$(eval ${cmd})
    echo ${val}
}

# Gets SSH password via hidden input
function getSSHPassword () {
  host=$1
  user=$2
  hiddenAskFor "Password for ${user} in ${host}"
}

USER_SNS='"stefano.fogarollo"'
DOMAIN_SNS="sns"
HOST_SNS=$(getSSHHost ${DOMAIN_SNS})
alias sshSNS='connect-vpnc sns; loginSSH ${USER_SNS} ${HOST_SNS} $(getSSHPassword ${HOST_SNS} ${USER_SNS})'

alias sshSNSRoot='connect-vpnc sns; loginSSH ${R} ${HOST_SNS} $(getSSHPassword ${HOST_SNS} ${R})'

USER_ZIH='"stfo194b"'
DOMAIN_ZIH="zih"
HOST_ZIH=$(getSSHHost ${DOMAIN_ZIH})
alias sshZIH='loginSSH ${USER_ZIH} ${HOST_ZIH} $(getSSHPassword ${HOST_ZIH} ${USER_ZIH})'

USER_OVH=${R}
DOMAIN_OVH="me.ovh"
HOST_OVH=$(getSSHHost ${DOMAIN_OVH})
alias sshOvh='loginSSH ${USER_OVH} ${HOST_OVH} $(getSSHPassword ${HOST_OVH} ${USER_OVH})'
