# kernel 5.11

MAINLINE_URL="https://kernel.ubuntu.com/~kernel-ppa/mainline"

# download
wget -c ${MAINLINE_URL}/v5.11/amd64/linux-headers-5.11.0-051100_5.11.0-051100.202102142330_all.deb
wget -c ${MAINLINE_URL}/v5.11/amd64/linux-headers-5.11.0-051100-generic_5.11.0-051100.202102142330_amd64.deb
wget -c ${MAINLINE_URL}/v5.11/amd64/linux-image-unsigned-5.11.0-051100-generic_5.11.0-051100.202102142330_amd64.deb
wget -c ${MAINLINE_URL}/v5.11/amd64/linux-modules-5.11.0-051100-generic_5.11.0-051100.202102142330_amd64.deb

# install all
sudo dpkg -i *.deb
