# Systemctl 

# start service
alias uStart='systemctl --user start'

# status service
alias uStat='systemctl --user status'

# stop service
alias uStop='systemctl --user stop'

# re-start service
alias uRes='systemctl --user restart'

# Systemctl (analogous to service)

# start service
alias sStart='sudo systemctl start'

# status service
alias sStat='sudo systemctl status'

# stop service
alias sStop='sudo systemctl stop'

# re-start service
alias sRes='sudo systemctl restart'

