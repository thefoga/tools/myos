echo "You may need to adjust GRUB settings ... "
echo "GRUB_CMDLINE_LINUX_DEFAULT=\"acpi_osi=! acpi_backlight=native idle=nomwait quiet splash\""
echo "... and then run \`sudo update-grub\`"

echo "I am also installing lightdm ... as it's the best ... and you should set it as default DE"
aptitude install lightdm

