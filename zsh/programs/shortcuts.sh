# vim
alias vi=$(which vim)

# python
alias py2=$(which python2)
alias py3=$(which python3)

# calculator
alias calc='bc -l'

alias o='xdg-open'

# ~/.zshrc
alias editZsh='vim ${HOME}/.zshrc'
alias reloadZsh='source ${HOME}/.zshrc'
alias c='code .'  # opens Visual Studio Code here
