function _wav2mp3 () {
  inFile=$1
  outFile=${inFile}.mp3
  ffmpeg -i ${inFile} -vn -acodec libmp3lame -ac 2 -ab 320k -ar 48000 ${outFile}
}
alias wav2mp3='_wav2mp3'

function _mp42mp3 () {
  inFile=$1
  outFile=${inFile}.mp3
  ffmpeg -i ${inFile} -vn -acodec libmp3lame -ac 2 -ab 320k -ar 48000 ${outFile}
}
alias mp42mp3='_mp42mp3'

function _mp32mp4 () {
  inFile=$1
  imgFile=$2
  outFile=${inFile}.mp4

  ffmpeg -loop 1 -i ${imgFile} -i ${inFile} -shortest -c:v libx264 -preset ultrafast -c:a copy ${outFile}
}
alias mp32mp4='_mp32mp4'

# usage: _videoConvert <filename> <extension>
# example: _videoConvert movie.mkv mp4
function _videoConvert () {
  inFile=$1
  extension=$2
  outFile=${inFile}.${extension}

  ffmpeg -i ${inFile} ${outFile}
}
alias videoConvert='_videoConvert'
