export CREDENTIALS_FOLDER=${HOME}/Documents/hackme/credentials
export SSH_CREDENTIALS=${CREDENTIALS_FOLDER}/ssh.json
export FTP_CREDENTIALS=${CREDENTIALS_FOLDER}/ftp.json
export OTHER_CREDENTIALS=${CREDENTIALS_FOLDER}/settings.json

# Gets value of key in settings file
# Usage: getJsonVal <key>
# Example: getSettingsVal '."a"."b"'
# Depends on: getJsonVal
function _getSettingsVal () {
    key=$1
    cmd="getJsonVal ${key} ${OTHER_CREDENTIALS}"
    val=$(eval ${cmd})
    echo ${val}
}
alias getSettingsVal='_getSettingsVal'

# Gets password of user in domain
# Usage: getPassword <domain> <user> <credentials file>
# Example: getPassword '."me"."home"."rasp"' 'pi' ${SSH_CREDENTIALS}
# Depends on: getPassword
function _getPassword () {
    domain=$1
    user=$2
    credentials=$3
    key='.${domain}.${user}."pass"'
    cmd="getJsonVal ${key} ${credentials}"
    val=$(eval ${cmd})
    echo ${val}
}
alias getPassword='_getPassword'

# Gets host of domain
# Usage: getHost <domain> <credentials file>
# Example: getHost '."me"."home"."rasp"'
# Depends on: getJsonVal
function _getHost () {
    domain=$1
    credentials=$2
    key='.${domain}."host"'
    cmd="getJsonVal ${key} ${credentials}"
    val=$(eval ${cmd})
    echo ${val}
}
alias getHost='_getHost'

# Gets password via hidden input
function hiddenAskFor () {
  question=$1
  local tmp

  echo ${question}": " >&2
  read -s tmp
  echo ${tmp}
}