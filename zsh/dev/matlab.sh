function _purgeMatlabCrashLogs () {
  REGEX="matlab_crash_dump"
  for f in $(ag -g ${REGEX}); do echo ${f} & rm ${f}; done;
}
alias purgeMatlabCrashLogs='_purgeMatlabCrashLogs'
