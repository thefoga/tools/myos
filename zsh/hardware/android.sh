function backupAndroidPics () {
    root_folder="/run/user/1000/gvfs"
    mobile_folder=$(ls /run/user/1000/gvfs | head -n1)
    mobile_folder=${root_folder}"/"${mobile_folder}

    screenshots_folder=${mobile_folder}"/Internal shared storage/DCIM/Screenshots"
    pic_folder=${mobile_folder}"/Samsung SD card/DCIM/Camera"

    local_folder=${HOME}"/Pictures/Android/"
    mkdir ${local_folder}

    cp ${screenshots_folder} ${local_folder}
    cp ${pic_folder} ${local_folder}

    echo "Copied from "${mobile_folder}" to "${local_folder}" !"
}

