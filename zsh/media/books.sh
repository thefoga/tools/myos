# searches my books collection
BOOKS_FOLDER=${HOME}/Books

alias _searchBook='cd ${BOOKS_FOLDER} && _findFiles'

# Depends on: _findFiles
alias searchComputerBook='cd ${BOOKS_FOLDER}/computer-science && _searchBook'

# Depends on: _findFiles
alias searchMathsBook='cd ${BOOKS_FOLDER}/mathematics && _searchBook'

# Depends on: _findFiles
alias searchLiteratureBook='cd ${BOOKS_FOLDER}/literature && _searchBook'
