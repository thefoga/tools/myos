export SCRATCH_FS=${HOME}/scratch

function _makeScratch () {
  folder=$1
  alreadyThere=$(find ${SCRATCH_FS} -maxdepth 1 -name ${folder} -type d | wc -l)
  if [[ ${alreadyThere} == 1 ]] ; then
    echo "Cannot make a scratch. Already a folder with same name!"
  else
    mv ${folder} ${SCRATCH_FS}
    ln -s ${SCRATCH_FS}/${folder}
  fi
}
alias makeScratch='_makeScratch'

alias showScratch='l ${SCRATCH_FS}'
