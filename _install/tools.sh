source ./meta/apt.sh

packages=(
    aptitude
    gdebi
    gnome-clocks
    fdupes
    lm-sensors
    sensors-detect
    vnstat
    sshpass
    pandoc
    xclip
    # clamav
    # clamtk
    jq
    notify-send
    htop
    partitionmanager
    pdftk
    calibre
    kde-spectacle
)

for package in "${packages[@]}"
do
  aptInstall ${package}
done

sudo freshclam  # updates clamav db

ytFeed=${HOME}/.local/bin/ytFeed.py
ytFeedUrl=https://gitlab.com/thefoga/tools/pymisc/-/raw/master/youtube/youtube_feed.py
wget -O ${ytFeed} ${ytFeedUrl} 
chmod +x ${ytFeed}

echo "Install exa: https://the.exa.website/#installation !!!"
echo "Download AdNauseam: https://github.com/dhowe/AdNauseam/releases !!!"

# sudo snap install keepassxc
