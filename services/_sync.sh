source "../_helpers/meta.sh"
source "../_helpers/links.sh"

THIS_FOLDER=$(whereIam)
TARGET_FOLDER=${HOME}/.config/systemd/user

# Symbolic link of . files in service folder from this folder
function here2ServiceFile {
  dotFile=$1
  fullDotFilePath=${THIS_FOLDER}/${dotFile}
  dest=${TARGET_FOLDER}/${dotFile}

  mkdir -p ${TARGET_FOLDER}
  linkFile ${fullDotFilePath} ${dest}
}

here2ServiceFile jupyter.service
# here2ServiceFile plasma_sess.service
# here2ServiceFile newtab.service

systemctl --user daemon-reload
systemctl --user restart jupyter.service
# systemctl --user restart plasma_sess.service
