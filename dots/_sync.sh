source "../_helpers/meta.sh"
source "../_helpers/links.sh"

THIS_FOLDER=$(whereIam)

# Symbolic link of . files in home folder from this folder
function here2HomeDotfile {
  dotFile=$1
  fullDotFilePath=${THIS_FOLDER}/${dotFile}
  dest=${HOME}/${dotFile}

  linkFile ${fullDotFilePath} ${dest}
}

here2HomeDotfile .vimrc
here2HomeDotfile .zshrc
here2HomeDotfile .gitconfig
here2HomeDotfile .tmux.conf
