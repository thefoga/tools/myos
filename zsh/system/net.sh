# blockst host
function blockHost () {
  host=$1
  hostsFile=/etc/hosts
  echo "0.0.0.0 ${host}" | sudo tee -a ${hostsFile}
}

# shows each program network connection (and spee)
alias netHogs='sudo nethogs -a'

# shows open ports
alias lntp='netstat -tunapl'

# shows nearby Wi-Fis
alias showWifis='nmcli device wifi list'

# test network
# Explanation: 1.1.1.1 is the fastest (as of EOY 2019)
alias testPing='ping 1.1.1.1'

# get IP info
IPSTACK_API_KEY=$(echo $(getSettingsVal '."ipstack"."api"') | sed 's/"//g')
alias ipInfo='/bin/bash ${OS_CONFIG_FOLDER}/bin/ipstack.sh ${IPSTACK_API_KEY}'

# shows info about network connections/adapters
alias ips='ip a'

# shows current ARP table
alias arpTable='arp -na'

# local network IP
alias ipLocal="ifconfig | ag -w inet | awk '{print \$2}'"

# shows IP of server
alias ipOf='dig +short'

# show processes listening to port
function _showListeners () {
  port=$1
  netstat -plnt | fgrep ${port}
}
alias showListeners='_showListeners'

# view devices in connected to IP
function _whoIsConnected () {
  ip=$1
  nmap -sn ${ip}/24
}
alias whoIsConnected='_whoIsConnected'

# find raspberry-pi IP
alias findRaspberry='arp -na | grep -i b8:27:eb'

alias netInterface="route | grep -m1 ^default | awk '{print \$NF}'"
