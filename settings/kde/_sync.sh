source "../../_helpers/meta.sh"
source "../../_helpers/links.sh"

THIS_FOLDER=$(whereIam)
CONFIGS_FOLDER=./config

function syncShortcuts {
  targetFolder=${HOME}/.config

  cd ${CONFIGS_FOLDER}
  fileName="khotkeysrc"
  linkFileFromHere ${fileName} ${targetFolder}/${fileName}
  cd -
}

function syncDolphinBookmarks {
  targetFolder=~/.local/share

  cd ${CONFIGS_FOLDER}
  fileName="user-places.xbel"
  linkFileFromHere ${fileName} ${targetFolder}/${fileName}
  cd -
}

syncShortcuts
syncDolphinBookmarks
