# regular text
TXT_BLACK='\033[0;30m'
TXT_RED='\033[0;31m'
TXT_GREEN='\033[0;32m'
TXT_YELLOW='\033[0;93m'
TXT_BLUE='\033[0;34m'
TXT_PURPLE='\033[0;35m'
TXT_CYAN='\033[0;96m'
TXT_WHITE='\033[0;37m'

# Bold
BOLD_BLACK='\033[1;30m'
BOLD_RED='\033[1;31m'
BOLD_GREEN='\033[1;32m'
BOLD_YELLOW='\033[1;33m'
BOLD_BLUE='\033[1;34m'
BOLD_PURPLE='\033[1;35m'
BOLD_CYAN='\033[1;36m'
BOLD_WHITE='\033[1;37m'

# underline
UND_BLACK='\033[4;30m'
UND_RED='\033[4;31m'
UND_GREEN='\033[4;32m'
UND_YELLOW='\033[4;33m'
UND_BLUE='\033[4;34m'
UND_PURPLE='\033[4;35m'
UND_CYAN='\033[4;36m'
UND_WHITE='\033[4;37m'

# Background
BAK_BLACK='\033[40m'
BAK_RED='\033[41m'
BAK_GREEN='\033[42m'
BAK_YELLOW='\033[43m'
BAK_BLUE='\033[44m'
BAK_PURPLE='\033[45m'
BAK_CYAN='\033[46m'
BAK_WHITE='\033[47m'

# Text Reset
COL_RESET='\033[0m'

function _echoWithColor () {
  color=$1
  text=$2
  echo -e "${color}${text}${COL_RESET}" > /dev/tty
}
alias echoWithColor='_echoWithColor'
alias echoSuccess='_echoWithColor ${TXT_GREEN}'
alias echoWarning='_echoWithColor ${TXT_YELLOW}'
alias echoFail='_echoWithColor ${TXT_RED}'
