JULIA_VERSION=$(curl -s "https://api.github.com/repos/JuliaLang/julia/releases/latest" | grep -Po '"tag_name": "v\K[0-9.]+')
JULIA_MINOR_VERSION=$(echo $JULIA_VERSION | grep -Po "^[0-9]+.[0-9]+")

URL='https://julialang-s3.julialang.org/bin/linux/x64/${JULIA_MINOR_VERSION}/julia-${JULIA_VERSION}-linux-x86_64.tar.gz'
OUT='julia.tar.gz'

curl -o ${OUT} ${URL}
tar -xvzf ${OUT}

# sudo cp -r ${VERSION} ${INSTALL_FOLDER}
# sudo ln -s /opt/${VERSION}/bin/julia /usr/local/bin/julia
# rm ${OUT}
