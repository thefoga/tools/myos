# from https://github.com/ohmyzsh/ohmyzsh#basic-installation
sh -c "$(curl -fsSL https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"

# installs ZSH plugins

GIT_CLONE="git clone"
INSTALL_FOLDER=${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins
mkdir -p ${INSTALL_FOLDER}

${GIT_CLONE} https://github.com/zsh-users/zsh-autosuggestions ${INSTALL_FOLDER}/zsh-autosuggestions
${GIT_CLONE} https://github.com/popstas/zsh-command-time ${INSTALL_FOLDER}/command-time
${GIT_CLONE} https://github.com/t413/zsh-background-notify ${INSTALL_FOLDER}/zsh-background-notify

cd ~/.oh-my-zsh/themes && wget https://raw.githubusercontent.com/oskarkrawczyk/honukai-iterm/master/honukai.zsh-theme
