# preprend this to enlarge QT_SCREEN_SCALE_FACTORS=1

desktop_env=$(echo $XDG_CURRENT_DESKTOP | awk -F: '{ print $2}')
desktop_gnome="GNOME"
desktop_kkde="KDE"

echo $desktop_env

if [ "$desktop_env" = "$desktop_gnome" ]; then
  /usr/bin/terminator --profile stefano --layout default
else
  /usr/bin/konsole --profile stefano --hide-menubar --show-tabbar
fi

#/usr/bin/konsole --profile stefano --hide-menubar --show-tabbar
#/usr/bin/terminator --profile stefano --layout default
#gnome-terminal

