# extract subtitles from movie
function _extractSubtitles () {
  filename=$1
  subline=`mkvmerge -i "$filename" | grep -i "subtitles"`
  tracknumber=`echo $subline | egrep -o "[0-9]{1,2}" | head -1`
  mkvextract tracks "$filename" $tracknumber:"subs.srt"
}
alias extractSubtitles='_extractSubtitles' 
