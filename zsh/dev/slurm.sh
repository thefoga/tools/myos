alias slurmShowQueue='squeue -u ${USER}'
alias slurmCancellAllJobs='scancel -u ${USER}'
alias slurmShowQueueProj='squeue -A'

alias slurmCurrentJob='slurmShowQueue | tail -n 1 | awk "{print \$1}"'
alias slurmCurrentErr='tail -n20 $(slurmLatestJob).err'
alias slurmCurrentOut='tail -n20 $(slurmLatestJob).out'

alias slurmLatestJob="find . -maxdepth 1 -type f -iname \*.out | sort | tail -n1 | sed 's/.out//g' | sed 's/\.\///g'"
alias slurmLatestErr='cat $(slurmLatestJob).err && echo "\n-- reading from $(slurmLatestJob)"'
alias slurmLatestOut='cat $(slurmLatestJob).out && echo "\n-- reading from $(slurmLatestJob)"'

alias findBatch='find . -maxdepth 1 -type f -iname \*.sbatch | sort | head -n1'
alias slurmLaunchSbatch='nodestat | tail -n4 | head -n3 && head -n10 $(findBatch) && sbatch $(findBatch) && squeue -u $USER'

# usage: showClusterUsageInMonth "p_humanpose"
function _slurmShowUsageInMonth () {
    account=$1
    allocated_cpu_hours=3500

    day=$(date +"%d")
    month=$(date +"%m")
    year=$(date +"%Y")
    firstOfThisMonth=${year}-${month}-01
    daysInMonth=30

    bc_precision="scale=3"

    cpu_minutes=$(sreport cluster AccountUtilizationByUser Accounts=${account} Start=${firstOfThisMonth} | tail -n1 | awk '{print $5}')
    cpu_hours=$(echo "${bc_precision};${cpu_minutes}/60.0" | bc)
    as_perc=$(echo "${bc_precision};${cpu_hours}/${allocated_cpu_hours}*100.0" | bc)
    predicted_by_end=$(echo "${bc_precision};${daysInMonth}/${day}*${cpu_hours}" | bc)
    predicted_as_perc=$(echo "${bc_precision};${predicted_by_end}/${allocated_cpu_hours}*100.0" | bc)

    echo "${cpu_hours} CPU-hours used (since ${firstOfThisMonth}, ${as_perc} % of max)"
    echo "${predicted_by_end} CPU-hours will be used (by EOM, at this rate, ${predicted_as_perc} % of max)"
}
alias slurmShowUsageInMonth='_slurmShowUsageInMonth'
