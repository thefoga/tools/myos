function handleLastReturnCode () {
	cmd=$1
  exitCode=$?
  
	if [ $exitCode -eq 0 ];then
	   echo ""
  else
	   echo "${cmd} did not complete successfully. Take a look at the .log file"
	fi
}

function whereIam () {
  currentFolder="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
  echo ${currentFolder}
}
