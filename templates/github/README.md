<div align="center">
	<h1>My Awesome project</h1>
	<em>Description of my awesome project</em></br>

<a href="https://landscape.io/github/MY_USERNAME/MY_REPOSITORY/master"><img src="https://landscape.io/github/MY_USERNAME/MY_REPOSITORY/master/landscape.svg?style=flat"></a> <a href="https://codeclimate.com/github/MY_USERNAME/MY_REPOSITORY"><img src="https://lima.codeclimate.com/github/MY_USERNAME/MY_REPOSITORY/badges/gpa.svg"></a> <a href="https://www.python.org/download/releases/3.4.0/"><img src="https://img.shields.io/badge/Python-3.5-blue.svg"></a></br>

<a href="https://github.com/MY_USERNAME/MY_REPOSITORY/pulls"><img src="https://badges.frapsoft.com/os/v1/open-source.svg?v=103"></a> <a href="https://github.com/MY_USERNAME/MY_REPOSITORY/issues"><img src="https://img.shields.io/badge/contributions-welcome-brightgreen.svg?style=flat"></a> <a href="https://app.fossa.io/projects/git%2Bhttps%3A%2F%2Fgithub.com%2FMY_USERNAME%2FMY_REPOSITORY?ref=badge_shield"><img src="https://app.fossa.io/api/projects/git%2Bhttps%3A%2F%2Fgithub.com%2FMY_USERNAME%2FMY_REPOSITORY.svg?type=shield"></a> <a href="https://opensource.org/licenses/Apache-2.0"><img src="https://img.shields.io/badge/License-Apache%202.0-blue.svg"></a> <a href="https://opensource.org/licenses/MIT"><img src="https://img.shields.io/badge/License-MIT-blue.svg"></a> <a href="http://unlicense.org/"><img src="https://img.shields.io/badge/license-Unlicense-blue.svg"></a>
</div>

## Install
```shell
$ 
```
*To install from source*


## Usage
```shell
$ 
```
*See the [documentation](/docs) for more information*


## Got questions?

If you have questions or general suggestions, don't hesitate to submit
a new [Github issue](https://github.com/MY_USERNAME/MY_REPOSITORY/issues/new).


## Contributing
[Fork](https://github.com/MY_USERNAME/MY_REPOSITORY/fork) | Patch | Push | [Pull request](https://github.com/MY_USERNAME/MY_REPOSITORY/pulls)


## Feedback
Suggestions and improvements [welcome](https://github.com/MY_USERNAME/MY_REPOSITORY/issues)!


## Authors

| [![MY_USERNAME](https://avatars0.githubusercontent.com/u/14162628?s=128&v=4)](https://github.com/MY_USERNAME "Follow @MY_USERNAME on Github") |
|---|
| [Stefano Fogarollo](https://MY_USERNAME.github.io) |


## Thanks
Thanks to anyone who ever loved this awesome project. 


## License
[Apache License](http://www.apache.org/licenses/LICENSE-2.0) Version 2.0, January 2004
[MIT License](https://opensource.org/licenses/MIT)
[Unlicense](https://unlicense.org/)
