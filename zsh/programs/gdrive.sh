export GOOGLE_DRIVE_FS=${HOME}/GDrive

alias gdrivePull='cd ${GOOGLE_DRIVE_FS} && drive pull && cd -'
alias gdrivePush='cd ${GOOGLE_DRIVE_FS} && drive push && cd -'
alias gdriveUpdate='pullGDrive && pushGDrive'
