# show processes by name
alias procs='ps -aux | ag'

# kill process
alias abort='kill -9'

# check the total memory usage of processes
function _checkMem () {
  program=$1
  pids=$(pidof ${program})
  
  memories=$(for p in ${=pids}; do cat /proc/$p/status | grep -i vmrss | awk '{print $2}'; done)
  
  sum=0
  for m in ${=memories}
  do
    sum="${sum} + ${m}"
  done

  totSum=$(echo "(${sum}) / 1000.0" | bc)
  echo "${totSum} MB"
}
alias checkMem='_checkMem'

function _psTree () {
  program=$1
  pstree $(pidof -s ${program})
}
alias psTree='_psTree'
