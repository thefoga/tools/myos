# better ls
function _getExa () {
    # todo auto loc=${HOME}/.cargo/bin/exa
    loc=$MY_BIN/exa-linux-x86_64
    echo "${loc} -a --long --header --git"
}
alias l="$(_getExa)"
alias ll='ls -a -l --color'

# pretty PATH
alias prettyPath='echo $PATH | sed -e "s/:/\\n/g"'

# cp/mv/rm
alias cp='cp -rv'

alias mv='mv -v'
alias mkdir='mkdir -pv'

# make necessary folders, then cd
function _mkdircd () {
  new_folder="$@"
  mkdir ${new_folder} && cd ${new_folder};
}
alias m='_mkdircd'  # mkdirs necessary

alias rm='rm -rfv'

# wget
alias wget='wget -c'

# find stuff

# Finds stuff in text of files in current folder recursively
# Usage: _findTextInFolder <stuff to search for> <folder to search>
# Depends on: ag
function _findTextInFolder () {
  stuffToSearch=$1
  folderToSearch=$2
  ag --hidden ${stuffToSearch} ${folderToSearch}
}

# Depends on: _findTextInFolder
function _findTextHere () {
  stuffToSearch=$1
  _findTextInFolder ${stuffToSearch} .
}

# Same as _findTextHere but also open the first result with vim
# Depends on: ag, vim
function _imFeelingLucky () {
  stuffToSearch=$1
  folderToSearch=$2

  allResults=$(ag -l ${stuffToSearch} ${folderToSearch})
  nResults=$(echo ${allResults} | wc -c)

  if [[ ${nResults} -gt 1 ]]
  then
    firstResult=$(echo ${allResults} | head -n 1)
    echo ${allResults}
    echo "opening ${firstResult} ..."

    vim ${firstResult} -c "/${stuffToSearch}"  # open vim and search for word
  else
    echo "No results!"
  fi

}

function _findMultipleTextInFile () {
  stuffToSearch=$1
  fileToSearch=$2
  linesToSearch=5  # todo default

  _cmd="import sys;
keywords = sys.argv[1].split(' ');
file_to_search = keywords[-2];
lines_to_search = keywords[-1];
keywords = keywords[:-2];
first_cmd = 'grep -5 {} {}'.format(keywords[0], file_to_search);
others_cmd = ['grep {}'.format(x) for x in keywords[1:]];
all_cmd = [first_cmd] + others_cmd;
piped_cmd = ' | '.join(all_cmd);
print(piped_cmd)"

  _exe=$(python3 -c $_cmd "$stuffToSearch $fileToSearch $linesToSearch")
  results=$(eval $_exe)
  echo $results | sed 's/$/\n/'  # add new line
}

alias fin='_findTextInFolder'
alias ft='_findTextHere'
alias ifl='_imFeelingLucky'
alias fts='_findMultipleTextInFile'

# Finds files matching query in current directory
# Usage: _findFiles <query>. Example: _findFiles "java design patterns"
# Depends on: python, ag
function _findFiles () {
  stuffToSearch=$1
  _cmd="import sys;
keywords = sys.argv[1].split(' ');
first_cmd = 'ag --hidden -g {}'.format(keywords[0]);
others_cmd = ['ag --hidden {}'.format(x) for x in keywords[1:]];
all_cmd = [first_cmd] + others_cmd;
piped_cmd = ' | '.join(all_cmd);
print(piped_cmd)"  # first is find, then piped
  _exe=$(python3 -c $_cmd "$stuffToSearch")
  results=$(eval $_exe)  # evaluate
  nResults=$(echo $results | tee /dev/tty | wc -l)  # print and count lines
  if [[ ${nResults} -gt 1 ]]
  then
    echo "\nFound ${nResults} results in $PWD"  # details
  fi
}
alias ff='_findFiles'

# Removes exec permissions from all files in directory
# Depends on: find
function _rmExec () {
  folder=$1
  find ${folder} -type d -exec chmod a-x,u+x -R "{}" \;  # folders executables by user ONLY
  find ${folder} -type f -exec chmod a-x "{}" \;  # NOT executable files recursively
}
alias rmExec='_rmExec'

# change directories easily
alias ..='cd ..'
alias ...='cd ../..'
alias ....='cd ../../..'
alias .....='cd ../../../..'

# extract
# Extracts archive.
# Usage: _extract <path/file_name>
function _extract () {
  if [[ "$#" -lt 1 ]]; then
    echoWarning "Usage: extract <path/file_name>.<zip|rar|bz2|gz|tar|tbz2|tgz|Z|7z|xz|ex|tar.bz2|tar.gz|tar.xz>"
    return 1 #not enough args
  fi

  if [[ ! -e "$1" ]]; then
    echoFail "File does not exist!"
    return 2 # File not found
  fi

  filename=`basename "$1"`
  extension="${filename##*.}"
  filename="${filename%.*}"
  DESTDIR=$(pwd)/${filename}
  mkdir -p ${DESTDIR}

  case ${extension} in
    tar)
    echo -e "Extracting $1 to $DESTDIR: (uncompressed tar)"
    tar xvf "$1" -C "$DESTDIR"
    ;;
    gz)
    echo -e "Extracting $1 to $DESTDIR: (gip compressed tar)"
    tar xvfz "$1" -C "$DESTDIR"
    ;;
    tgz)
    echo -e "Extracting $1 to $DESTDIR: (gip compressed tar)"
    tar xvfz "$1" -C "$DESTDIR"
    ;;
    xz)
    echo -e "Extracting  $1 to $DESTDIR: (gip compressed tar)"
    tar xvf -J "$1" -C "$DESTDIR"
    ;;
    bz2)
    echo -e "Extracting $1 to $DESTDIR: (bzip compressed tar)"
    tar xvfj "$1" -C "$DESTDIR"
    ;;
      tbz2)
      echo -e "Extracting $1 to $DESTDIR: (tbz2 compressed tar)"
      tar xvjf "$1" -C "$DESTDIR"
    ;;
    zip)
    echo -e "Extracting $1 to $DESTDIR: (zipp compressed file)"
    unzip "$1" -d "$DESTDIR"
    ;;
    lzma)
      echo -e "Extracting $1 : (lzma compressed file)"
    unlzma "$1"
    ;;
    rar)
    echo -e "Extracting $1 to $DESTDIR: (rar compressed file)"
    unrar x "$1" "$DESTDIR"
    ;;
    7z)
    echo -e  "Extracting $1 to $DESTDIR: (7zip compressed file)"
    7za e "$1" -o "$DESTDIR"
    ;;
    xz)
      echo -e  "Extracting $1 : (xz compressed file)"
      unxz  "$1"
      ;;
    exe)
       cabextract "$1"
      ;;
    *)
    echoFail "Unknown archieve format!"
    return
      ;;
  esac
}
alias extract='_extract'

# Compress a folder/file and removes it
# Depends on: tar
function _tarThis () {
    stuffToCompress=$1
    out=${stuffToCompress}.tar.gz
    tar -cvzf ${out} ${stuffToCompress}
    rm ${stuffToCompress}
}
alias tarThis='_tarThis'
alias tarAll='for i in $(ls); do echo $i && tarThis $i; done'

# Unzips all zip archives in current folder
alias unzipAll='for i in *.zip; do yes | unzip $i; done'

# Adds .bak extension
function _addBak () {
  stuff=$1
  new_stuff=${stuff}.bak
  mv -v ${stuff} ${new_stuff}
}
alias addBak='_addBak'

# Copy into .bak extension
function _mkBak () {
  stuff=$1
  new_stuff=${stuff}.bak
  cp -rf ${stuff} ${new_stuff}
}
alias mkBak='_mkBak'

# Removes .bak extension from *.bak
# Usage: _removeBak <folder>
# Depends on: ag
function _removeBak () {
  stuff=$1
  new_stuff=$(basename ${stuff} .bak)
  mv -v ${stuff} ${new_stuff}
}
alias rmBak='_removeBak'

# Finds different files (just names) between 2 folders
# Usage: _diffFolders <first folder> <second folder>
# Depends on: ag
function _diffFolders () {
  first=$(mktemp tmp.XXXXXX)  # create tmp
  ag -g . $1 | sort > ${first}
  sed -i "s=$1==" ${first}  # just relative path

  second=$(mktemp tmp.XXXXXX)
  ag -g . $2 | sort > ${second}
  sed -i "s=$2==" ${second}  # remove first and last line

  diff ${first} ${second} | ag '[<>]'  # hide line number

  rm ${first} ${second}  # clean
}
alias diffFolders='_diffFolders'

# Replaces words in folder
# Usage: _replaceIn <old word> <new word> <folder>
# Depends on: find, sed
function _replaceIn () {
  old=$1
  new=$2
  folder=$3
  for f in $(find ${folder} -type f)
  do
    cmd="sed -i -e 's/${old}/${new}/g' ${f}"
    echo ${cmd}
    eval ${cmd}
  done
}
alias replaceIn='_replaceIn'

alias clearDownloads='rm -rf ${MY_DOWNLOADS}/*'

alias howManyFiles='ls -1q * | wc -l'

function randomFile () {
    files=$(find . -mindepth 1 -type f)
    echo $files | shuf -n 1
}

alias cleanTrash='rm -rf ~/.local/share/Trash/*'
alias treeWithSize='tree -hF'

function _purgeRootLogs () {
  sudo su

  find /var/log -type f -name "*.gz" -exec rm -f {} \;
  sudo /etc/cron.daily/logrotate
  rm -rf /var/log/user.log
  rm -rf /var/log/syslog
  rm -rf /var/log/messages

  du -H /var/log
  rm -rf /var/log/journal/e3946049ff124f2380df2df8bfdfce00
  du -H /var/log
}
alias purgeRootLogs='_purgeRootLogs' 
