sudo apt install vim-gtk3

# Vundle plugin manager
INSTALL_FOLDER=$HOME/.vim/bundle

mkdir -p ${INSTALL_FOLDER}
cd ${INSTALL_FOLDER}
git clone https://github.com/VundleVim/Vundle.vim.git
vim +PluginInstall +qall
