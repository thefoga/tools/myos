source ./meta/apt.sh

# install winehq
sudo dpkg --add-architecture i386
wget -nc https://dl.winehq.org/wine-builds/winehq.key
sudo apt-key add winehq.key
rm winehq.key

sudo add-apt-repository 'deb https://dl.winehq.org/wine-builds/ubuntu/ focal main'
aptUpdate
sudo apt install --install-recommends winehq-stable winetricks

GARMIN_FOLDER=${HOME}/opt/GarminExpress
WINEPREFIX=${GARMIN_FOLDER} winetricks dotnet452 vcrun2010 corefonts
WINEPREFIX=${GARMIN_FOLDER} winetricks win7

wget https://download.garmin.com/omt/express/GarminExpress.exe
WINEPREFIX=${GARMIN_FOLDER} wine explorer /desktop=garmin,1366x768 ./GarminExpress.exe
