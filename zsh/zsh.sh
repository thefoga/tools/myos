export ALIAS_FILE_EXTENSION="sh"  # extension of aliases file
export ALIAS_SRC_RUNNER="_run"

# Wrapper of `source`: do not exit if file not found, simply show warning
# Usage: sourceOrFail <file path>
function sourceOrFail () {
    sourceFile=$1

    if [ -f ${sourceFile} ]; then
        source ${sourceFile}
    else
        echo "[WARNING]: REQUESTED SOURCE FILE "${sourceFile} "CANNOT BE FOUND!!!"
    fi
}

# Sources every .sh files found (recursively) in folder
function sourceFolder () {
    folder=$1

    # source anything ending with .sh
    for source_f in $(ag -g ".sh" ${folder})
    do
        sourceOrFail ${source_f}  # defined in main .bashrc (.zshrc)
    done
}

# Sources a file in folder
# Usage: sourceFileInFolder <file name> <folder name>
# Example: sourceFileInFolder admin/apt .
function sourceFileInFolder () {
    relativePath=$1
    fullPath=$2/${relativePath}
    sourceOrFail ${fullPath}
}

# Sources a file in user's alias folder
# Usage: sourceAliasFile <file name>
# Example: sourcealiasFile admin/apt
function sourceAliasFile () {
    relativePath=$1"."${ALIAS_FILE_EXTENSION}  # add extension
    sourceFileInFolder ${relativePath} ${WHERE_ARE_MY_ALIASES}
}

function sourceAllAliases () {
    sourceAliasFile ${ALIAS_SRC_RUNNER}
}

sourceAllAliases
