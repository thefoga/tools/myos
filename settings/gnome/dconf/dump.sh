source ./common.sh

for conf in "${configs[@]}"
do
  pat=$(config2path ${conf})
  fil=$(config2file ${conf})

  dconf dump ${pat} > ${fil}
done
