# coding: utf-8

from pathlib import Path
import numpy as np
import json
from datetime import datetime, timedelta


def get_tomorrow_at_00(now=datetime.now()):
    today_at_00 = datetime(now.year, now.month, now.day)

    tomorrow = now + timedelta(days=1)
    tomorrow_at_00 = datetime(tomorrow.year, tomorrow.month, tomorrow.day)

    return today_at_00, tomorrow_at_00


def get_first_of_next_month(now=datetime.now()):
    first_of_this_month = datetime(now.year, now.month, 1)

    n_days = 1
    first_of_next_month = first_of_this_month + timedelta(days=n_days)
    while first_of_next_month.month == first_of_this_month.month:
        n_days += 1
        first_of_next_month = first_of_this_month + timedelta(days=n_days)

    return first_of_this_month, first_of_next_month, n_days


def get_first_of_next_quarter(now=datetime.now()):
    def _quarter(dt):
        return (dt.month - 1) // 4  # 0-based

    def _first_of_quarter(dt):
        q = _quarter(dt)
        month = 4 * q + 1
        return datetime(year=dt.year, month=month, day=1)

    first_of_this_month, first_of_next_month, _ = get_first_of_next_month(now)

    while _quarter(first_of_next_month) == _quarter(first_of_this_month):
        _, first_of_next_month, _ = get_first_of_next_month(now=first_of_next_month)

    first_of_this_quarter = _first_of_quarter(now)
    n_days = (first_of_next_month - first_of_this_quarter).total_seconds() / (24 * 60 * 60)
    return first_of_this_quarter, first_of_next_month, n_days


class Deadline:
    dt_format = '%Y-%m-%d %H:%M'

    def __init__(self, dt, description):
        self.dt = datetime.strptime(dt.strip(), self.dt_format)
        self.description = description.strip()

    def count_seconds(self):
        delta_t = self.dt - datetime.now()
        return delta_t.total_seconds()

    def count_hours(self, allow_sleep=True, allow_weekends=True):
        delta_seconds = self.count_seconds()
        delta_hours = delta_seconds / (60 * 60)

        if not allow_weekends:
            delta_hours *= 5 / 7

        if not allow_sleep:
            delta_hours *= (24 - 8) / 24.0

        return delta_hours

    def count_days(self, allow_weekends=True):
        delta_seconds = self.count_seconds()
        delta_days = delta_seconds / (24 * 60 * 60)

        if not allow_weekends:
            delta_days *= 5.0 / 7.0

        return delta_days

    def __str__(self):
        f_out = '{} due on {}'
        return f_out.format(
            self.description,
            self.dt.strftime(self.dt_format)
        )


class Countdown:
    dt_format = '%Y-%m-%d %H:%M'
    days_format = '%Y-%m-%d'

    def __init__(self, days, description, from_dt=None):
        self.days = float(days)
        self.description = description.strip()

        if from_dt:
            self.from_dt = datetime.strptime(from_dt.strip(), self.dt_format)
        else:
            self.from_dt = datetime.now()

        self.dt = self.from_dt + timedelta(days=self.days)

    def get_done(self, as_perc=True):
        total = (self.dt - self.from_dt).total_seconds()
        done = (datetime.now() - self.from_dt).total_seconds()
        ratio = done / total
        left = 1 - ratio

        if as_perc:
            ratio *= 100.0
            left = 100 - ratio

        return ratio, left

    def get_stats(self):
        ratio, left = self.get_done(as_perc=False)

        if self.days <= 1:
            total = self.days * 24
            time_name = 'hours'
        elif self.days < 365:
            total = self.days
            time_name = 'days'
        else:  # self.days >= 365
            total = self.days / 365.0
            time_name = 'years'

        return ratio * 100.0, left * 100.0, ratio * total, left * total, time_name

    def __str__(self):

        if self.days <= 1:
            f_out = '{} will happen {:.1f} hours from {}, on {}'
            return f_out.format(
                self.description,
                self.days * 24,
                self.from_dt.strftime(self.dt_format),
                self.dt.strftime(self.dt_format)
            )
        elif self.days < 365:
            f_out = '{} will happen {:.0f} days from {}, on {}'
            return f_out.format(
                self.description,
                self.days,
                self.from_dt.strftime(self.days_format),
                self.dt.strftime(self.days_format)
            )
        else:  # self.days >= 365
            f_out = '{} will happen {:.1f} years from {}, on {}'
            return f_out.format(
                self.description,
                self.days / 365.0,
                self.from_dt.strftime(self.days_format),
                self.dt.strftime(self.days_format)
            )

    @staticmethod
    def end_of_day():
        today_at_00, tomorrow_at_00 = get_tomorrow_at_00()
        from_dt = today_at_00.strftime(Countdown.dt_format)
        return Countdown(1, 'End Of Day', from_dt=from_dt)

    @staticmethod
    def end_of_month():
        first_of_this_month, _, n_days = get_first_of_next_month()
        from_dt = first_of_this_month.strftime(Countdown.dt_format)
        return Countdown(n_days, 'End Of Month', from_dt=from_dt)

    @staticmethod
    def end_of_quarter():
        first_of_this_quarter, _, n_days = get_first_of_next_quarter()
        from_dt = first_of_this_quarter.strftime(Countdown.dt_format)
        return Countdown(n_days, 'End Of Quarter', from_dt=from_dt)

    @staticmethod
    def end_of_year():
        now = datetime.now()
        capodanno = datetime(year=now.year, month=1, day=1)
        eoy = datetime(year=now.year, month=12, day=31, hour=23, minute=59, second=59)
        n_days = (eoy - capodanno).total_seconds() / (24 * 60 * 60)
        from_dt = capodanno.strftime(Countdown.dt_format)
        return Countdown(n_days, 'End Of Year', from_dt=from_dt)

    @staticmethod
    def from_dict(x):
        if x['days'] == 'EOD':
            return Countdown.end_of_day()
        elif x['days'] == 'EOM':
            return Countdown.end_of_month()
        elif x['days'] == 'EOQ':
            return Countdown.end_of_quarter()
        elif x['days'] == 'EOY':
            return Countdown.end_of_year()

        if not ('from_dt' in x):
            x['from_dt'] = None


        return Countdown(
            eval(x['days']),
            x['description'],
            from_dt=x['from_dt']
        )


def get_progress_bar(done_ratio, max_bars=80):
    if done_ratio < 0:
        done_ratio = 0

    if done_ratio > 1:
        done_ratio = 1

    done_bars = int(np.round(done_ratio * max_bars))
    left_bars = max_bars - done_bars

    return '[' + '=' * done_bars + ' ' * left_bars + ']'


def center_in_line(line_length):
    f = '{:^' + '{:.0f}'.format(line_length) + '}'

    def _f(x):
        return f.format(x)
    return _f


def print_countdowns(countdowns, title, line_length=80):
    if countdowns:
        print(center_in_line(line_length)(title))
        print()

        for c in countdowns:
            done_perc, left_perc, done, left, time_name = c.get_stats()
            done_string = 'done: {:.0f} % ({:.1f} {})'.format(done_perc, done, time_name)
            left_string = 'left: {:.0f} % ({:.1f} {})'.format(left_perc, left, time_name)

            print('- {}'.format(str(c)))
            print('  {}'.format(get_progress_bar(done_perc / 100.0, max_bars=94)))
            print('  {:46}'.format(done_string) + '{:>50}'.format(left_string))


def print_deadlines(deadlines, title, line_length=80):
    if deadlines:
        print(center_in_line(line_length)(title))
        print()

        for d, relaxed in deadlines:
            print('- {}'.format(str(d)))
            if relaxed:
                print('    - days left: {:.1f}'.format(d.count_days(allow_weekends=True)))
                print('    - hours left: {:.1f}'.format(d.count_hours(allow_sleep=True, allow_weekends=True)))
            else:  # job-type of deadlines => not working on it on weekends/nights
                print('    - days left (not counting weekends): {:.1f}'.format(d.count_days(allow_weekends=False)))
                print('    - hours left (not counting weekends and sleep): {:.1f}'.format(d.count_hours(allow_sleep=False, allow_weekends=False)))


def print_stuff(deadlines, countdowns, print_std=True, line_length=80):
    if print_std:
        stds = [
            Countdown.end_of_day(),
            Countdown.end_of_month(),
            Countdown.end_of_quarter(),
            Countdown.end_of_year()
        ]
        print_countdowns(stds, 'Standard countdowns', line_length=line_length)
        # print()

    # print_countdowns(countdowns, 'Countdowns', line_length=line_length)
    # print()

    # print_deadlines(deadlines, 'Deadlines', line_length=line_length)
    # print()


def parse_file(f_path):
    with open(f_path, 'r') as reader:
        data = json.load(reader)

    deadlines = [
        (
            Deadline(d['dt'], d['description']),
            d['relaxed']  # bool
        )
        for d in filter(lambda x: x['visible'], data['deadlines'])
    ]

    countdowns = [
        Countdown.from_dict(c)
        for c in filter(lambda x: x['visible'], data['countdowns'])
    ]

    return deadlines, countdowns


def main(f_path=Path.home() / 'data' / 'times.json'):
    try:
        deadlines, countdowns = parse_file(f_path)
    except:
        deadlines, countdowns = [], []

    print_stuff(deadlines, countdowns, line_length=100)


if __name__ == '__main__':
    main()
