function _findFolders () {
  folder=$1
  cmd="find ${folder} -mindepth 1 -maxdepth 1 -type d"
  eval ${cmd}
}
alias findFolders='_findFolders'

function _weightOf () {
  folder=$1
  du -h ${folder} | sort -hr
}
alias weightOf='_weightOf'  # size of this folder
alias goBack='cd - &> /dev/null'
