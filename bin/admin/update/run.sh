source ../../../_helpers/meta.sh

THIS_FOLDER="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
JOB_FOLDER=${THIS_FOLDER}/jobs
JOB_LOGS=${THIS_FOLDER}/logs
JOBS=(
    "apt"  # apt packages
    "snap"  # snap packages
    "pip"  # python, pip2 and pip3 global packages
    "youtubedl"  # youtube-dl
)

rm -rf ${JOB_LOGS}  # clean log folder
mkdir -p ${JOB_LOGS}

for job in ""${JOBS[@]}""
do
  printf "$job ..."
  bash ${JOB_FOLDER}/${job}.sh 2>&1 | tee ${JOB_LOGS}/$job.log
  handleLastReturnCode $job
done
