source ./meta/apt.sh

packages=(
    plasma-discover
    elisa
    kmines
    kajongg
    konversation
    ksudoku
    kpat kgames
    kmahjongg
    thunderbird
    transmission-gtk
    gnome-screenshot
)

for package in "${packages[@]}"
do
  aptRemove ${package}
done

apt autoremove
