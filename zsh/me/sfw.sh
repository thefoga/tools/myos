function setVolume2 () {
  vol=$1
  amixer -D pulse sset Master ${vol}% &> /dev/null
}

# e.g `abortFirstMatch "tor browser"`
function abortFirstMatch () {
  searchString=$1
  foundPid=$(ps -ef | ag ${searchString} | head -n1 | awk '{print $2}')
  kill -9 ${foundPid} &> /dev/null
}

# e.g `showNotifications true`
function showNotifications () {
  value=$1
  gsettings set org.gnome.desktop.notifications show-banners ${value}
}

function setLockScreenTimeoutMinutes () {
  minutes=$1
  seconds=$(expr ${minutes} \* 60)
  gsettings set org.gnome.desktop.session idle-delay ${seconds}
}

function sfwMode () {
  setVolume2 5
  abortFirstMatch "tor browser"
  showNotifications false
  setLockScreenTimeoutMinutes 1
}

function nsfwMode () {
  # well, if the user wants a low vol .. setVolume2 75
  showNotifications true
  setLockScreenTimeoutMinutes 60  # 1h
}

function getWiFiSSID () {
  iwgetid -r
}

# ideally run as cronjob..
function checkSfwOnWiFi () {
  safe_connections=(
      SCHNELL_5Ghz
      hackme  # todo
  )

  current_wifi=$(getWiFiSSID)
  if [[ ! " ${safe_connections[*]} " =~ " ${current_wifi} " ]]; then
      # showNotifications true && notify-send "Go to work now !"
      sfwMode  # current Wi-Fi is NOT safe!
  else
      nsfwMode  # current Wi-Fi should be safe ..
      # notify-send "NSFW .."
  fi
}

#checkSfwOnWiFi  # will run on EVERY terminal opening .. maybe disable those `notify-send` ..
