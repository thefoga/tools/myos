<div align="center">
	<h1>My awesome project</h1>
	<em>This is an example file with default selections</em></br>

<a href="https://travis-ci.org/GITHUB_USER/GITHUB_REPO"><img src="https://travis-ci.org/GITHUB_USER/GITHUB_REPO.svg?branch=master"></a> <a href="https://ci.appveyor.com/project/GITHUB_USER/GITHUB_REPO"><img src="https://ci.appveyor.com/api/projects/status/isfmmdaqhkbgqaeu?svg=true"></a> <a href="https://coveralls.io/github/GITHUB_USER/GITHUB_REPO?branch=master"><img src="https://coveralls.io/repos/github/GITHUB_USER/GITHUB_REPO/badge.svg?branch=master"></a></br>

<a href="https://landscape.io/github/GITHUB_USER/GITHUB_REPO/master"><img src="https://landscape.io/github/GITHUB_USER/GITHUB_REPO/master/landscape.svg?style=flat"></a> <a href="https://codeclimate.com/github/GITHUB_USER/GITHUB_REPO"><img src="https://lima.codeclimate.com/github/GITHUB_USER/GITHUB_REPO/badges/gpa.svg"></a> <a href="tbd"><img src="https://mperlet.de/pybadge/badges/8.83.svg"></a></br>

<a href="https://pypi.org/project/GITHUB_REPO/"><img src="https://badge.fury.io/py/GITHUB_REPO.svg"></a> <a href="https://requires.io/github/GITHUB_USER/GITHUB_REPO/requirements/?branch=master"><img src="https://requires.io/github/GITHUB_USER/GITHUB_REPO/requirements.svg?branch=master"></a> <a href="http://GITHUB_REPO.readthedocs.io/en/latest/?badge=latest"><img src="https://readthedocs.org/projects/GITHUB_REPO/badge/?version=latest"></a></br>

<a href="https://en.wikipedia.org/wiki/Linux"><img src="https://img.shields.io/badge/platform-macOS%2C%20Linux%20%26%20Windows-blue.svg"></a>
</div>

## Table of content

- [Key Features](#key-features)
- [Install](#install)
- [Building](#building)
- [Usage](#usage)
- [Examples](#examples)
- [Changelog](#changelog)
- [Contribute](#contribute)
- [License](#license)
- [FAQ](#faq)
- [Links](#links)
- [You may also like...](#you-may-also-like)

## Key Features

* KEY_FEATURE_0
	- KEY_SUBFEATURE_0

## Install

```bash
$
```
*To install from source*

## Usage

```bash
$
```
*See the [documentation](/docs) for more information*

### Supported commands

```bash
$  --help
```

The following flags are supported:

| Flag | Default | Description |
| --- | --- | --- |
| `in` | n/a | Input file |
| `out` | n/a | Output file |
| `blur` | 4 | Blur radius |
| `max` | 2500 | Maximum number of points |
| `noise` | 0 | Noise factor |
| `points` | 20 | Points threshold |
| `sobel` | 10 | Sobel filter threshold |
| `solid` | false | Solid line color |
| `wireframe` | 0 | Wireframe mode (without,with,both) |
| `width` | 1 | Wireframe line width |


## Examples

#### Example 0
#### Output
```text
```


## Got questions?

If you have questions or general suggestions, don't hesitate to submit
a new [Github issue](https://github.com/MY_USERNAME/MY_REPOSITORY/issues/new).


## Changelog
See [CHANGELOG](https://github.com/GITHUB_USER/GITHUB_REPO/blob/master/CHANGELOG.md)


## Contribute

[![Contributions welcome](https://img.shields.io/badge/contributions-welcome-brightgreen.svg?style=flat)](https://github.com/GITHUB_USER/GITHUB_REPO/issues) [![Open Source Love](https://badges.frapsoft.com/os/v1/open-source.svg?v=103)](https://opensource.org/licenses/Apache-2.0)

0. [Open an issue](https://github.com/GITHUB_USER/GITHUB_REPO/issues/new)
0. [fork](https://github.com/GITHUB_USER/GITHUB_REPO/fork) this repository
0. create your feature branch (`git checkout -b my-new-feature`)
0. commit your changes (`git commit -am 'Added my new feature'`)
0. publish the branch (`git push origin my-new-feature`)
0. [open a PR](https://github.com/GITHUB_USER/GITHUB_REPO/compare)


## License

[![FOSSA Status](https://app.fossa.io/api/projects/git%2Bhttps%3A%2F%2Fgithub.com%2FGITHUB_USER%2FGITHUB_REPO.svg?type=shield)](https://app.fossa.io/projects/git%2Bhttps%3A%2F%2Fgithub.com%2FGITHUB_USER%2FGITHUB_REPO?ref=badge_shield)
[![License](https://img.shields.io/badge/License-Apache%202.0-blue.svg)](https://opensource.org/licenses/Apache-2.0) [![License](https://img.shields.io/badge/License-MIT-blue.svg)](https://opensource.org/licenses/MIT) [![License](https://img.shields.io/badge/license-Unlicense-blue.svg)](http://unlicense.org/)

[Apache License](http://www.apache.org/licenses/LICENSE-2.0) Version 2.0, January 2004
[MIT License](https://opensource.org/licenses/MIT)
[Unlicense](https://unlicense.org/)


## FAQ
### FIRST_FAQ_QUESTION
FIRST_FAQ_ANSWER


## Links

* [Web site](https://GITHUB_USER.github.io)
* [Issue tracker](https://github.com/GITHUB_USER/GITHUB_REPO/issues)
* [Source code](https://github.com/GITHUB_USER/GITHUB_REPO)
* [Email](mailto:MY_EMAIL)


## You may also like...

- [MY_OTHER_REPO_0](https://github.com/GITHUB_USER/MY_OTHER_REPO_0) - MY_OTHER_REPO_0_DESCRIPTION
- [MY_OTHER_REPO_1](https://github.com/GITHUB_USER/MY_OTHER_REPO_1) - MY_OTHER_REPO_1_DESCRIPTION


## Credits

- Concept and development by [this guy](http://github.com).
- Design by [this other guy](http://github.com).
- Repository maintained by [@guy0](http://github.com), [@guy1](http://github.com) and [@guy2](http://github.com).
- This project includes [contributions by many fine folks](https://github.com/GITHUB_USER/GITHUB_REPO/contributors).