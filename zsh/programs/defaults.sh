export TERMINAL_EDITOR=$(which vim)
export GUI_EDITOR=$(which subl)

alias te='${TERMINAL_EDITOR}'
alias ge='${GUI_EDITOR}'
alias geMakefile='ge Makefile'
