#!/usr/bin/env python3
# -*- coding: utf-8 -*-

""" Backup/load GNOME 3 keyboard shortcuts """

import argparse
from enum import Enum
from pathlib import Path
from gi.repository import Gio
import json
import os


class RunningMode(Enum):
    to_file = 1
    from_file = 2

SHORTCUT_KEYS = ['name', 'command', 'binding']


def create_args():
    """
    :return: ArgumentParser
        Parser that handles cmd arguments.
    """

    parser = argparse.ArgumentParser(
        usage='-m <running mode> -f <.json file> -h for full usage'
    )
    parser.add_argument(
        '-m', dest='mode', help='running mode', required=True
    )
    parser.add_argument(
        '-f', dest='file_path', help='.json file to use', required=True
    )
    return parser


def parse_args(parser):
    """
    :param parser: ArgumentParser
        Object that holds cmd arguments.
    :return: tuple
        Values of arguments.
    """

    args = parser.parse_args()

    # parse
    mode = str(args.mode)
    file_path = str(args.file_path)

    # assert
    assert RunningMode[mode]

    folder_of_file = Path(file_path).parent
    if not os.path.exists(folder_of_file):
        os.makedirs(folder_of_file)

    return {
        'mode': RunningMode[mode],
        'file_path': file_path
    }


def backup(file_path, schema='org.gnome.settings-daemon.plugins.media-keys'):
    out = []

    settings = Gio.Settings(schema=schema)
    custom_keybindings = settings.get_value('custom-keybindings')

    for key_binding in custom_keybindings:
        c = Gio.Settings.new_with_path(schema + '.custom-keybinding', key_binding)

        out_schema = {
            key: str(c.get_string(key))
            for key in SHORTCUT_KEYS
        }
        out.append(out_schema)

    out = {
        'shortcuts': out
    }

    with open(file_path, 'w') as writer:
        json.dump(out, writer)


def from_file(file_path, schema='org.gnome.settings-daemon.plugins.media-keys', key='custom_{}'):
    keybindings = None
    with open(file_path, 'r') as reader:
        keybindings = json.load(reader)

    if keybindings:
        all_keybindings = []  # keep track
        key_binding_path = '/' + schema.replace('.', '/') + '/custom-keybindings/' + key + '/'

        for i, keybinding in enumerate(keybindings['shortcuts']):
            binding_path = key_binding_path.format(i)
            c = Gio.Settings.new_with_path(
                schema + '.custom-keybinding',
                binding_path
            )

            for key in SHORTCUT_KEYS:
                c.set_string(key, keybinding[key])

            all_keybindings.append(binding_path)
            Gio.Settings.sync()

        settings = Gio.Settings(schema=schema)
        custom_keybindings = settings.set_strv('custom-keybindings', all_keybindings)


def main():
    args = parse_args(create_args())
    if args['mode'] == RunningMode.to_file:
        backup(args['file_path'])
    elif args['mode'] == RunningMode.from_file:
        go_on = input('This will destroy current keybindings, continue? [y/n]')
        if go_on.startswith('y'):
            from_file(args['file_path'])
    else:
        error_message = '{} not available'.format(args['mode'].name)
        raise NotImplementedError(error_message)


if __name__ == '__main__':
    main()
