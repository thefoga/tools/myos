MATLAB_VERION=R2020a
MATLAB_LOCATION="/usr/local/MATLAB/${MATLAB_VERION}/bin/matlab"
alias matlab="${MATLAB_LOCATION} -nodesktop"
