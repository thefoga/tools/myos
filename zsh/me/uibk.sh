export MY_UIBK_PC="138.232.106.181"

# before ssh
alias vpnUIBK="tail -n1 ~/PhD/config/vpn.ovpn | awk '{print \$3}' | xclip -selection clipboard && echo 'Password copied into clipboard: insert it then zsh ~/PhD/config/keep_me_logged_in.sh' && /opt/cisco/anyconnect/bin/vpnui"
alias abortCISCO="sudo kill -9 $(ps -aux | ag cis | head -n1 | awk '{ print $2}')"
alias sshUIBK='ssh stefano@${MY_UIBK_PC}'

# from ssh connection
alias startJupyterConnectionFromExtWithUIBK="ssh -NL 1299:127.0.0.1:1299 stefano@${MY_UIBK_PC}" 

# from workstation
alias startJupyterConnectionForExt='source ~/opt/jupyterhub/.venv/bin/activate && jupyter notebook --no-browser --port 1299 --notebook-dir=/home/stefano/opt/jupyterhub/_notebooks'

# from within VPN
alias mountIFIShare='sudo mount -t cifs -o username=c7031381,domain=UIBK //ifishare/igs /mnt/ifishare/igs'
alias mountNAS='sudo mount -t cifs //138.232.65.197/scans /mnt/nas -o user=stefano'
