alias mk='make -j 4'

export CLANG_STYLE="Chromium"
alias doClangFormat="clang-format -i -style=${CLANG_STYLE}"

function _doClang () {
  folder=$1
  cd ${folder}

  C_FILES=(
    "*.h"
    "*.c"
  )
  CPP_FILES=(
    "*.cpp"
    "*.hpp"
  )
  files=($(echo ${CPP_FILES[*]}) $(echo ${C_FILES[*]}))
  
  for f in "${files[@]}"
  do
    cmd="doClangFormat ${f}"
    eval ${cmd}
  done
  cd -
}
alias doClang='_doClang'

alias diff='diff -iEZbwB'
alias cloc='cloc . --by-percent c'
alias delta='diff --color'

# from https://docs.fedoraproject.org/en-US/quick-docs/creating-and-using-a-live-installation-image/
function createUSBDisk () {
    echo "WARNING: this is a way-too-risky thing to automate, homo faber fortunae suae"
    echo "1) dd --list"
    echo "2) umount /dev/sdX*, where X is the appropriate letter"
    echo "3) sudo dd if=/path/to/image.iso of=/dev/sdX bs=8M status=progress oflag=direct && sudo sync"
}
