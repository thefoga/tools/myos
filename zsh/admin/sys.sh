# runs some system diagnostics
function _report () {
  echo "\n- uptime"
  uptime

  echo "\n- logged users"
  who

  echo "\n- architecture"
  arch

  echo "\n- # processors"
  nproc

  echo "\n- disks"
  lsblk

  echo "\n- USB devices"
  lsusb

  echo "\n- GPUs"
  lshw -C display
}
alias report='_report'

alias purgeLogs='sudo truncate -s 0 /var/log/syslog && du -h /var/log'  # https://askubuntu.com/a/1014350
alias purgePipCache='rm .cache/pip'

alias fixHostnameResolution='echo "nameserver 1.1.1.1" | sudo tee /etc/resolv.conf'
