export GIT_AUTO_COMMIT="myos-generated commit"

function _gitShowCurrentBranch () {
  git branch | grep \* | cut -d ' ' -f2
}
alias gitShowCurrentBranch=_gitShowCurrentBranch

alias gitShowOrigin="git remote -v | awk '{print \$2; exit}'"
alias gitLsLocalBranches="git branch | sed 's|* |  |' | sort"
alias gitLsRemoteBranches="git ls-remote -h $(gitShowOrigin) | awk '{print $2}' | sed 's:refs/heads/::')"
alias findGits='find . -type d -iname .git'
alias gitLsContributors="git log --format='%aN <%ce>' | sort -u"
alias gitShowLastCommit='echo $(git log --format="%H" -n 1)'  # otherwise it's interactive..

function _gitCloneShallow () {
  gitUrl=$1
  git clone ${gitUrl} --depth=1
  cd "$(basename "$gitUrl" .git)"
}
alias gitCloneShallow='_gitCloneShallow'

function _gitCloneInScratch () {
  gitUrl=$1
  git clone ${gitUrl}
  clonedFolder=$(basename "$gitUrl" .git)
  makeScratch ${clonedFolder}
  cd ${clonedFolder}
}
alias gitCloneInScratch='_gitCloneInScratch'

function _gitRmLocalBranches () {
  git reset --hard
  git gc --aggressive
  git prune
  git clean -fd
}
alias gitRmLocalBranches='_gitRmLocalBranches'

function _gitFixIDE () {
  files=(
    .idea
    .vscode
  )

  for f in ${files[@]}
  do
    git rm -rf --cached ${f}
  done
}
alias gitFixIDE='_gitFixIDE'

alias gitShowConflicts='tmp=$(git diff --name-only --diff-filter=U); echo ${tmp}'
alias gitShowDiff='git diff'

function _gitSearchContentHistory () {
  text=$1

  git rev-list --all | GIT_PAGER=cat xargs git grep ${text}
}
alias gitSearchContentHistory='_gitSearchContentHistory'

function _gitSearchCommitMessageFor () {
  text=$1
  since=$2
  until=$3

  git log --grep=${text} --since ${since} --until ${until}
}
alias gitSearchCommitMessageFor='_gitSearchCommitMessageFor'

function _gitHardPull () {
    current_branch=$(_gitShowCurrentBranch)

    git reset --hard ${current_branch}
    git pull origin ${current_branch} || git pull origin/${current_branch}  # paths
}
alias gitHardPull='_gitHardPull'

_findInRecentGit() {
    searchString=$1
    maxDaysOld=$2
    extension=$3

    today=$(date -d $(date +%Y-%m-%d) '+%s')
    filesWithTODOs=$(ag --hidden ${searchString} . | ag ${extension} | awk -F":" '{split($0, a); print a[1]}' | uniq)

    echo ${filesWithTODOs} | while read file ; do
        lastModifiedGitCommit=$(git log -n 1 --oneline ${file} | awk '{print $1;}')
        commitDate=$(git show -s --format=%ci ${lastModifiedGitCommit} | head -n1 | awk '{print $1}')
        commitDateAsLinux=$(date -d ${commitDate} '+%s')
        diffDays=$(( (${today} - ${commitDateAsLinux}) / (60*60*24) ))

        if [[ ${diffDays} -lt ${maxDaysOld} ]]; then
            echo "${file}: $(grep -i ${searchString} ${file})"
        fi
    done
}

alias findRecentTODOInZettelkasten="_findInRecentGit '#todo' 30 .md | ag '#todo'"
