alias outputSoundThroughHeadphones='pactl set-sink-port 0 analog-output-headphones'
alias outputSoundThroughSpeaker='pactl set-sink-port 0 analog-output-speaker'

# https://askubuntu.com/a/230893/1522280 and https://askubuntu.com/a/230893
alias fixSoundNoise='pulseaudio -k && sudo alsa force-reload'
