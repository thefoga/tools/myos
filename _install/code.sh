source ./meta/apt.sh

packages=(
  # c
#  valgrind  # valgrind - memory manager tool
  pkg-config  # package manager for libraries
  doxygen # doxygen - Documentation system for C, C++, Java, Python and other languages
  build-essential libgl1-mesa-dev gdb  # qt
  gcc-avr binutils-avr avr-libc avrdude  # arduino sdk + IDE
  cmake

  # java
  openjdk-14-jre

  npm  # nodejs package manager# export NPM_INSTALL="sudo npm install -g "

  # ruby
#  ruby ruby-dev make gcc
#  jekyll bundler

  # sql
#  mysql-server  # mysql
#  sqlite3  # sqllite
#  sqlitebrowser  # browse .sql and .db

  # tools
  gitk
  cloc  # Count Lines Of Code
  lftp  # move files easily

  # tex
  texmaker texlive-full texlive-latex-extra
  texlive-fonts-recommended texlive-fonts-extra
  texlive-latex-recommended texlive-latex-extra
  texlive-pictures texlive-publishers

  dvipng
  hunspell hunspell-it hunspell-en-gb
  biber
)

for package in "${packages[@]}"
do
  aptInstall ${package}
done

# node v14 from 
curl -sL https://deb.nodesource.com/setup_14.x | sudo bash -
sudo apt -y install nodejs
node  -v  # just to verify

# yarn from https://classic.yarnpkg.com/lang/en/docs/install/#debian-stable
sudo npm install --global yarn
