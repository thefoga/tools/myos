source ./meta/apt.sh

# drive
# sudo add-apt-repository ppa:twodopeshaggy/drive
aptUpdate

packages=(
  # tools
  meld tree silversearcher-ag git vim

  # files
  unrar
  # drive

  # clipboard
  xclip

  # networking
  nmap curl

  # utils
  linux-tools-common linux-tools-generic linux-cloud-tools-generic exfat-utils terminator

  # cairo
  pkg-config libcairo2-dev gcc python3-dev libgirepository1.0-dev

  # db
  # mongo

  # optional
  fonts-firacode

  # prettifiers
  clang-format clang-tidy

  # https://gitlab.com/asus-linux/asusctl
  libclang-dev libudev-dev
)

for package in "${packages[@]}"
do
  aptInstall ${package}
done

# start mongo service
systemctl restart mongod
