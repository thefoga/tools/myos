alias clearNpmCache='sudo rm -rf /home/stefano/.npm'

# Shows runnable scripts in package.json
# Depends on: jq
function _showNpmScripts () {
  getJsonVal '.scripts' package.json
}
alias showNpmScripts='_showNpmScripts'
