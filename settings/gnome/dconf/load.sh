source ./common.sh

for conf in "${configs[@]}"
do
  pat=$(config2path ${conf})
  fil=$(config2file ${conf})

  dconf load ${pat} < ${fil}
done

echo "You may need to set:"
echo " - 'Switch windows' <- Super+Tab"
echo " - 'Switch applications' <- Alt+Tab"
