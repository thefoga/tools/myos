currentFolder="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd)"
reposFile=${currentFolder}/repos.txt

for d in $(find ./ -mindepth 1 -maxdepth 1 -type d)
do
    cd ${currentFolder}/${d}

    gitOrigin=$(git remote -v | awk '{print $2; exit}')

    if [[ ${#gitOrigin} -gt 0 ]] ; then
        echo $(pwd)' -> '${gitOrigin} 
        echo ${gitOrigin} >> ${reposFile}
    fi

    cd ..
done
