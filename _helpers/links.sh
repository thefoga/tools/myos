function doOrTrySudo () {
  cmd=$1
  eval ${cmd}
  exitCode=$?
  if [ $exitCode -ne 0 ] ; then
    cmd="sudo "${cmd}
    eval ${cmd}
    echo "OK! executed with sudo"
  fi
}

function backupStuff () {
  dateStr=$(date +%Y-%m-%d_%H-%M)  #  e.g 2019-09_03-22-11
  stuff=$1

  if [ -f "${stuff}" ]; then  # Existing file
    echo "Backing up existing file ${stuff}"
    doOrTrySudo "cp ${stuff}{,.${dateStr}}"
  elif [ -d "${dest}" ]; then  # Existing dir
    echo "Backing up existing folder ${dest}"
    doOrTrySudo "cp -r ${stuff}{,.${dateStr}}"
  fi
}

# Symbolic link of . file from src to destination. If file alredy exists, it backs it up
# Usage: linkFile <src> <dest>
function linkFile () {
  src=$1
  dest=$2

  dateStr=$(date +%Y-%m-%d_%H-%M)  #  e.g 2019-09_03-22-11

  if [ -f "${dest}" ]; then  # Existing file
    if [ ! -h "${dest}" ]; then  # it's a NOT a symlink
        echo "Backing up existing file ${dest}"
        mv ${dest}{,.${dateStr}}
    fi

  elif [ -d "${dest}" ]; then  # Existing dir
    echo "Backing up existing dir ${dest}"
    mv ${dest}{,.${dateStr}}
  fi

  echo "Creating new symlink: ${dest}"
  ln -fs ${src} ${dest}
}

# Symbolic link of . files in folder from this folder
function linkFileFromHere {
  dotFile=$1
  currentFolder="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
  fullDotFilePath=${currentFolder}/${dotFile}
  dest=$2

  linkFile ${fullDotFilePath} ${dest}
}
