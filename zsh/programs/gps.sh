function _fit2gpx () {
    filename=`basename "$1"`
    gpsbabel -i garmin_fit -f $1 -o gpx -F ${filename}.gpx
}
alias fit2gpx='_fit2gpx'
