# Prints message and loops audio file
# Depends on: cvlc
function printAndPlay () {
  echo $1 && cvlc --loop $2 2> /dev/null
}

alias playMusic='cvlc --loop'