alias excel2csv='for f in *.xlsx; do libreoffice --headless --convert-to csv "$f"; done'

function _csv2json () {
  inFile=$1
  cmd='python3 -c "import csv,json;print json.dumps(list(csv.reader(open(${inFile}))))"'
  eval ${cmd}
}
alias csv2json='_csv2json'
