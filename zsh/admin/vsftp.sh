# Adds new user to vsftp list
# Usage: _addNewVsftpdUser <username>
function _addNewVsftpdUser () {
  mkdir -p /home/$1/ftp
  adduser $1
  chown nobody:nogroup /home/$1/ftp
  chmod -R u=r /home/$1/ftp
  chmod a-w-x /home/$1/ftp
  chmod -R 755 /home/$1/ftp
  chown nobody:nogroup /home/$1/ftp
  chmod a-w-x /home/$1/ftp
  chown -R $1:$1 /home/$1/ftp
  chown -R $1:$1 /home/$1
  chmod a-w /home/$1/ftp
  chmod -R 755 /home/$1/ftp
  # add $1 to `/etc/vsftpd.userlist`
  service vsftpd restart
}
alias addNewVsftpdUser='_addNewVsftpdUser'
