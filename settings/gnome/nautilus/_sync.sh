source "../../_helpers/meta.sh"
source "../../_helpers/links.sh"

THIS_FOLDER=$(whereIam)

linkFileFromHere bookmarks ${HOME}/.config/gtk-3.0/bookmarks
linkFileFromHere user-dirs.dirs ${HOME}/.config/user-dirs.dirs
