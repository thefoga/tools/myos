# convert images in folder from .CR2 to .JPG in parallel (x8)
function _cr22jpg () {
  files=$1
  ls ${files} | parallel --will-cite --bar --jobs 8 ufraw-batch --out-type jpg --out-path jpg/ --overwrite
}
alias cr2jpg='_cr22jpg'

function _png2jpg () {
  inFile=$1
  outFile=${inFile}.jpg
  convert ${inFile} -quality 90 ${outFile}
}
alias png2jpg='_png2jpg'

function _mkTransparent () {
  inFile=$1
  outFile="trans"${inFile}
  gm convert ${inFile} -transparent white ${outFile}
}
alias mkTransparent='_mkTransparent'

function _optimizeImg () {
  files=$2
  extension="${files##*.}"

  case ${extension} in
    png)
    find -type f -name ${files} -exec optipng {} \;
    ;;
    jpg)
    find -type f -name ${files} -exec jpegoptim -f --max=80 --strip-all {} \;
    ;;
    jpeg)
    find -type f -name ${files} -exec jpegoptim -f --max=80 --strip-all {} \;
    ;;
    *)
    echo -e "Unknown image format!"
    return
      ;;
  esac
}
alias optimizeImg='_optimizeImg'

function _enhancePic () {
  inp=$1
  out=enhanced_${inp}

  convert -enhance -contrast ${inp} ${out}
}
alias enhancePic='_enhancePic'

function _reducePic () {
  inp=$1
  filename=$inp:t:r
  echo ${filename}
  extension="${inp##*.}"
  red=$2
  out=${filename}.small.${extension}

  convert ${inp} -resize ${red}% ${out}
  du -h ${out}
}
alias reducePic=_reducePic

alias jpg2gif='convert -resize 1080x19206 -delay 20 -loop 0 *.jpg out.gif'

function _downloadFromCanon () {
  folder=${HOME}/Pictures/"canon_pics_"$(date "+%Y-%m-%d_%H:%M:%S")
  mkdir -p ${folder}
  cd ${folder}

  gphoto2 --auto-detect
  gphoto2 --get-all-files

  echo "Downloaded pictures to ${folder}"
}


function _downloadFromGoPro () {
    in_folder=/media/${USER}/6139-6131/
    out_folder=${HOME}/Pictures

    cp ${in_folder}/DCIM/100GOPRO ${out_folder}

    cd ${out_folder}/100GOPRO

    rm *.THM

    mkdir preview
    mv *.LRV preview

    mkdir videos
    mv *.MP4 videos

    mkdir pics
    mv *.JPG pics
}
alias downloadFromGoPro='_downloadFromGoPro'
