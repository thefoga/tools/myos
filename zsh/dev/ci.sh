alias findTODO='_findTextHere "todo"'

function _findIssues () {
  if [ $# -eq 0 ]
  then
    searchFor="issue #"
  else
    issueNumber=$1
    searchFor="issue #"${issueNumber}
  fi
  
  _findTextHere ${searchFor}
}
alias findISSUES='_findIssues'
