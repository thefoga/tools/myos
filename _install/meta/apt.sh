alias APT_INSTALL="sudo apt-get install -y"
alias APT_UPDATE="sudo apt-get update"
alias APT_UPGRADE="sudo apt-get dist-upgrade"
alias APT_REMOVE="sudo apt-get purge -y"

function aptInstall () {
  package=$1
  cmd="$aliases[APT_INSTALL] ${package}"
  eval ${cmd}
}

function aptUpdate () {
  cmd="$aliases[APT_UPDATE]"
  eval ${cmd}
}

function aptUpgrade () {
  cmd="$aliases[APT_UPGRADE]"
  eval ${cmd}
}

function aptRemove () {
  package=$1
  cmd="$aliases[APT_REMOVE] ${package}"
  eval ${cmd}
}
