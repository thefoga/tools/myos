function _purgeFITs () {
    folder=$1
    backup_folder=$2

    mkdir -p ${backup_folder}
    cp -r ${folder} ${backup_folder}

    find ${folder} -maxdepth 1 -type f -iname \*.FIT -exec rm {} \;
}
alias cleanGarminWatch='_purgeFITs /media/stefano/GARMIN/GARMIN/ACTIVITY/ /home/stefano/backup/gc/ && _purgeFITs /media/stefano/GARMIN/GARMIN/MONITOR/ /home/stefano/backup/gc/'
