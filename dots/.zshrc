export PATH=$HOME/.cargo/env:${HOME}/.local/bin:${HOME}/bin:/usr/local/bin:/snap/bin:${PATH}

export ZSH="${HOME}/.oh-my-zsh"
export ZSH_THEME="honukai"
export UPDATE_ZSH_DAYS=14

# command-time config
export ZSH_COMMAND_TIME_COLOR="blue"

export EXECUTING_WHERE=$(pwd)
export EXECUTING_WHAT="init"
function _set_window_title () {
    title="\$ ${EXECUTING_WHAT} @ ${EXECUTING_WHERE}"
    echo -ne "\033]0;${title}\007"
}

# this function is called AFTER every "cd"
function chpwd() {
    ls -a -l --color .
    # EXECUTING_WHERE=$(pwd)
    # _set_window_title
}

# this function is called just before any command line is executed
function preexec {
    # EXECUTING_WHAT=$1
    # _set_window_title
}

export DISABLE_AUTO_TITLE="true"
function _get_prompt_char () {
    git branch >/dev/null 2>/dev/null && echo '±' && return
    hg root >/dev/null 2>/dev/null && echo '☿' && return
    echo '%* %#'
}

# my aliases
export OS_CONFIG_FOLDER=${HOME}/scratch/myos
export WHERE_ARE_MY_ALIASES=${OS_CONFIG_FOLDER}/zsh  # source folder of aliases

function _loadOhMyZsh () {
    source ${ZSH}/oh-my-zsh.sh
    unalias -a # stupid aliases

    # shows exit code
    setopt PRINT_EXIT_VALUE

    # automatically cd to a directory if it is given as command on the command line and it is not the name of an actual command
    setopt AUTO_CD

    plugins=(
        git
        command-time
    )

    source ${HOME}/.oh-my-zsh/custom/plugins/zsh-autosuggestions/zsh-autosuggestions.zsh
}

function _loadMyStuff () {
    source ${WHERE_ARE_MY_ALIASES}/zsh.sh

    # welcome message
    source ${WHERE_ARE_MY_ALIASES}/welcome.sh
}

_loadOhMyZsh
_loadMyStuff

# eval $(thefuck --alias)
#tmux list-sessions  # just to debug
