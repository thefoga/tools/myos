source "../_helpers/meta.sh"

THIS_FOLDER=$(whereIam)
ROOT_FOLDER=$(dirname ${THIS_FOLDER})  # up one

syncers=(
  bin
  dots
  services
  settings
)

for syncer in ${syncers}
do
  full_path=${ROOT_FOLDER}/${syncer}/_sync.sh
  wd=$(dirname ${full_path})
  chmod +x ${full_path}

  cd ${wd}
  zsh ${full_path}
  cd -
done

