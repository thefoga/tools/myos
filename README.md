My OS
========

*Central backup/restore/recovery point of the configuration files of my OS*

## Use case

Use when

- crash your Unix a few times a week
- manage multiple servers
- backup/restore your Unix config

## What's inside

- [scripts](bins/) to automate some of my work. Used in some services.
- [.desktop files](desktops/), like my `eclipse.desktop` and `matlab.desktop`
- [. files](dots/), like my `.vimrc`, `.zshrc`
- [various program settings](settings/), like `VS code`, `Sublime Text`
- [ZSH aliases](zsh/)
- [helpers/utils functions](_helpers/) to backup/restore the stuff

## Thanks

- [Algotech](https://github.com/algotech) for the original code
- [Victoria Drake](https://github.com/victoriadrake) for some of the bash aliases
