# !/bin/bash
# -*- coding: utf-8 -*-

JUPYTER_FOLDER=${HOME}/opt/jupyterhub
VENV_FOLDER=${JUPYTER_FOLDER}/.venv
NOTEBOOKS_FOLDER=${JUPYTER_FOLDER}/_notebooks
CONFIG_FILE=${JUPYTER_FOLDER}/jupyterhub_config.py

source ${VENV_FOLDER}/bin/activate
jt -T -f fira -nfs 14 -fs 14 -tfs 14 -cellw 100%  # theme
jupyter notebook --notebook-dir=${NOTEBOOKS_FOLDER} --config=${CONFIG_FILE}
