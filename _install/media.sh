source ./meta/apt.sh

packages=(
  vlc
  gimp
  optipng
  jpegoptim
  gnome-tweaks
  chrome-gnome-shell
  ffmpeg
  pdfgrep
  mkvtoolnix
  sox
  okular
)

for package in "${packages[@]}"
do
  aptInstall ${package}
done

curl -sS https://download.spotify.com/debian/pubkey_0D811D58.gpg | sudo apt-key add -
echo "deb http://repository.spotify.com stable non-free" | sudo tee /etc/apt/sources.list.d/spotify.list
sudo apt-get update && sudo apt-get install spotify-client

sudo curl -L https://yt-dl.org/downloads/latest/youtube-dl -o /usr/local/bin/youtube-dl
sudo chmod a+rx /usr/local/bin/youtube-dl

# https://inkscape.org/release/inkscape-1.0.2/gnulinux/ubuntu/ppa/dl/
sudo add-apt-repository ppa:inkscape.dev/stable
sudo apt update
sudo apt install inkscape
