function _mergePDF () {
  files=$1
  out=$2
  pdfunite ${files} ${out}
}
alias mergePDF='_mergePDF'

function _extractPDF () {
  file=$1
  pageStart=$2
  pageEnd=$3
  out=$4

  pdftk ${file} cat ${pageStart}-${pageEnd} output ${out}
}
alias extractPDF='_extractPDF'
