source "../_helpers/meta.sh"
source "../_helpers/links.sh"

THIS_FOLDER=$(whereIam)

function makeHosts {
  hostsFolder=./hosts
  hostExtension=.hosts
  targetFile=/etc/hosts

  backupStuff ${targetFile}
  sudo touch ${targetFile}
  cat ${hostsFolder}/*${hostExtension} | grep -v ^\# | uniq | sed -r '/^\s*$/d' | sudo tee ${targetFile} > /dev/null

  echo "Updated ${targetFile}"
}

function makeLink {
	dest_folder=$1
	file_name=$2

	mkdir -p ${dest_folder}
	linkFileFromHere ${file_name} ${dest_folder}/${file_name}

  echo "${file_name} -> ${dest_folder}"
}

# crontab
doOrTrySudo "sudo crontab crontab/root"
crontab crontab/stefano

makeLink ${HOME}/opt/jupyterhub jupyterhub_config.py
makeLink ${HOME}/.jupyter/nbconfig notebook.json
makeLink ${HOME}/.config/sublime-merge/Packages/User Preferences.sublime-settings

# Terminator
cd terminator
makeLink ${HOME}/.config/terminator config
cd -1

# Konsole
cd konsole
makeLink ${HOME}/.local/share/konsole stefano.profile
cd -1

makeHosts

# julia

JULIA_TARGET="${HOME}/.julia/config"
mkdir -p ${JULIA_TARGET}
linkFileFromHere startup.jl ${JULIA_TARGET}/startup.jl

# reminders that the following cannot be updated automatically

reminders=(
"Visual Studio Code"
"Jetbrains (Android Studio, PyCharm, CLion, WebStorm, IDEA)"
"Sublime Text"
)
echo ""
for reminder in ${reminders}
do
  echo "[!] ${reminder} cannot be updated automatically. Do it manually"
done

# todo.txt config
#mkdir -p ${HOME}/.todo/
#linkFileFromHere todo.cfg ${HOME}/.todo/config

# grub backup
#cat /boot/grub/grub.cfg >> ./grub.cfg
