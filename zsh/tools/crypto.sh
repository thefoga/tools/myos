function _searchHIVP () {
  pass=$1
  sha1=$(echo -n ${pass} | sha1sum | awk '{print toupper($0)}' |tr -d '\n')
  firstFive=$(echo -n $sha1 | cut -c1-5)
  lasts=$(echo -n $sha1 | cut -c6-40)
  curl -s -H $'Referer: https://haveibeenpwned.com/' https://api.pwnedpasswords.com/range/${firstFive} | grep -i ${lasts}
}
alias haveIBeenPwned='_searchHIVP'

function _randomPassword () {
  len=$1
  tr -dc A-Za-z0-9_ < /dev/urandom | head -c ${len} | xargs
}
alias randomPassword='_randomPassword 32'