alias plasmaSaveSession='qdbus org.kde.ksmserver /KSMServer org.kde.KSMServerInterface.saveCurrentSession'
#qdbus org.kde.ksmserver /KSMServer saveCurrentSession'
alias plasmaShowSessionSave='ls -lt ~/.config/session'
alias plasmaBackupSession='tar -cvzf ~/.config/session-$(date +"%Y-%m-%d_%H-%M-%S").tar.gz ~/.config/session'
