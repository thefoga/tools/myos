# update pip3
sudo apt install -y python3-pip --reinstall

# remove local pip installation
python3 -m pip uninstall --yes pip

# python 3
sudo pip3 freeze |  # display packages info
  grep -v '^\-e' |
  cut -d = -f 1 |  # get only name of packages
  xargs -n1 sudo pip3 install --upgrade  # actual forced upgrade ... and same thing with pip3
