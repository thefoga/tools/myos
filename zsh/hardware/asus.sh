# Depends on: https://gitlab.com/asus-linux/asusctl
function stopFan () {
    asusctl -f silent

    asusctl profile -t false
    asusctl profile -m 5
    asusctl profile -M 50
}

# Depends on: https://gitlab.com/asus-linux/asusctl
function normalCPU () {
    asusctl -f normal

    asusctl profile -t true
    asusctl profile -m 10
    asusctl profile -M 95
}

# Depends on: https://gitlab.com/asus-linux/asusctl
function turboCPU () {
    asusctl -f boost

    asusctl profile -t true
    asusctl profile -m 50
    asusctl profile -M 100
}

function setKeyboardLight () {
    level=$1
    brightness_file=/sys/class/leds/asus::kbd_backlight/brightness

    echo ${level} | sudo tee ${brightness_file}
    echo ${level} | sudo tee ${brightness_file}  # 2nd time just to be sure ..
}

function keyboardOff () {
    setKeyboardLight 0
}

function keyboardOn () {
    setKeyboardLight 4
}
