/**
 * Discard already present tables
*/
DROP TABLE IF EXISTS Awesome_Table;

/**
 * Create tables
*/
CREATE TABLE Awesome_Table (
	Id char(13) NOT NULL,
	Title varchar(50) NOT NULL,
	PRIMARY KEY (Id)
) ENGINE=InnoDB;


/**
 * insert values
*/
SET FOREIGN_KEY_CHECKS = 0;
SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";

INSERT INTO Awesome_Table(Id, Title) VALUES
("9780201038217", "The art of computer programming");
