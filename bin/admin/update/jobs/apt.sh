sudo aptitude update &&  # reload list of packages
  sudo aptitude -y full-upgrade &&  # reload list of distros updates
  sudo aptitude -y clean &&  # remove orphaned packages
  sudo aptitude -y autoclean  # clean cache
