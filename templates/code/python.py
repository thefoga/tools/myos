# !/usr/bin/python3
# coding: utf-8

import argparse


def create_args():
    """
    :return: ArgumentParser
        Parser that handles cmd arguments.
    """

    parser = argparse.ArgumentParser(usage="-o <option> "
                                           "-h for full usage")
    parser.add_argument("-o", dest="opt",
                        help="option", required=True)
    return parser


def parse_args(parser):
    """
    :param parser: ArgumentParser
        Object that holds cmd arguments.
    :return: tuple
        Values of arguments.
    """

    args = parser.parse_args()
    return args.opt


def main():
    """
    :return: void
        Main routine
    """

    args = parse_args(create_args())


if __name__ == "__main__":
    main()

