/**
 * Description of model
*/

set MODELS;
set RESOURCES;
param gains{MODELS};
param maxNumProducts{MODELS};
param requirements{RESOURCES, MODELS};
param available{RESOURCES};

var x{MODELS} integer >= 0;

maximize total_gain: sum{m in MODELS} x[m] * gains[m];
subject to max_resources{r in RESOURCES}: sum{m in MODELS} x[m] * requirements[r, m] <= available[r];