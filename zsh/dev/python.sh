# core
alias pipInstallReq='pip install -r requirements.txt'

# venv
export DEFAULT_VENV='.venv'

alias createVenv3='virtualenv -p /usr/bin/python3 ${DEFAULT_VENV}'
alias activateVenv='source ${DEFAULT_VENV}/bin/activate && which python'
alias stopVenv='deactivate'

alias createJupyterKernel='pip install ipykernel && python -m ipykernel install --user --name' 

alias pipFreeze='pip freeze >> requirements.txt'

# torch
alias torchCheckCuda='python3 -c "import torch; print(torch.cuda.is_available()); print(torch.cuda.get_device_name(0))"'

alias lsJupyterKernels='ls ~/.local/share/jupyter/kernels/'
