function _sendGMail () {
  sender=$1
  subject=$2
  recipient=$3
  msgFile=$4
  password=$(hiddenAskFor "GMail password for ${sender}")

  sendemail -l email.log     \
    -f ${sender}   \
    -u ${subject}     \
    -t ${recipient} \
    -s "smtp.gmail.com:587"  \
    -o tls=yes \
    -xu ${sender} \
	  -xp ${password} \
    -o message-file=${msgFile}
}
alias sendGMail='_sendGMail'