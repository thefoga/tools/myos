# !/bin/bash
# -*- coding: utf-8 -*-

# Gets info about public IP address using ipstack API
# Usage: ipstack.sh <API KEY> <optional: IP>
# Depends on: jq

# ipstack
IPSTACK_API_URL="http://api.ipstack.com/"
IPSTACK_API_KEY=$1  # reads API from input
IPSTACK_OPTIONS="?access_key=$IPSTACK_API_KEY"

# reads from input IP
if [ "$#" -gt 1 ]; then
  public_ip=$2  # second argument
else
  PUBLIC_IP_URL="http://ifconfig.me/ip/"
  public_ip=$(curl -s $PUBLIC_IP_URL)  # find out public IP
fi

## Google Maps
GMAPS_URL="http://maps.google.com/maps?q="

ipstack_call_url=$IPSTACK_API_URL$public_ip$IPSTACK_OPTIONS  # build API url
ip_info=$(curl -s $ipstack_call_url)  # call API
geo_lat=$(echo $ip_info | jq -r '.latitude')
geo_long=$(echo $ip_info | jq -r '.longitude')

echo $ip_info | jq .

gmaps_link=$GMAPS_URL$geo_lat","$geo_long
echo "Google Maps: "$gmaps_link
