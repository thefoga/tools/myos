[Appearance]
ColorScheme=Breeze
Font=Hack,14,-1,5,50,0,0,0,0,0

[Cursor Options]
CursorShape=0

[General]
Environment=TERM=xterm-256color,COLORTERM=truecolor
LocalTabTitleFormat=%n @ %d
Name=stefano
Parent=FALLBACK/
RemoteTabTitleFormat=%H
TerminalColumns=110
TerminalRows=28

[Interaction Options]
AutoCopySelectedText=true
CopyTextAsHTML=false
OpenLinksByDirectClickEnabled=false
UnderlineFilesEnabled=true

[Scrolling]
HistoryMode=2
ScrollBarPosition=2

[Terminal Features]
BlinkingCursorEnabled=true
