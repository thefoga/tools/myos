configs=(
  org/gnome/desktop/background
  org/gnome/desktop/session
  org/gnome/shell/app-switcher
  org/gnome/desktop/screensaver
  org/gnome/settings-daemon/plugins/color
  org/gnome/shell/window-switcher
  org/gnome/desktop/interface
  org/gnome/desktop/privacy
)

function config2file () {
  conf=$1
  fil=$(sed 's|/|.|g' <<< ${conf})
  echo ${fil}.dconf
}

function config2path () {
  conf=$1
  echo /${conf}/
}
