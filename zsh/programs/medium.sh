function _mediumRSS () {
  author=$1
  rssUrl="https://feedly.com/i/discover"  # feedly
  echo "https://medium.com/feed/${author}"
  xdg-open ${rssUrl}
}
alias mediumFeed='_mediumRSS'
