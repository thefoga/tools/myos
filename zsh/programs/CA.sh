function _createCA () {
    CA_NAME=$1
    working_folder=$(pwd)/CA_${CA_NAME}

    mkdir -p ${working_folder}
    cd ${working_folder}

    openssl genrsa -out ${CA_NAME}_key.pem 2048
    openssl req -new -sha256 -key ${CA_NAME}_key.pem -out ${CA_NAME}.csr  # this will ask details
    openssl req -x509 -sha256 -days 365 -key ${CA_NAME}_key.pem -in ${CA_NAME}.csr -out ${CA_NAME}_cert.pem
    openssl pkcs12 -export -out ${CA_NAME}.p12 -inkey ${CA_NAME}_key.pem -in ${CA_NAME}_cert.pem

    # at end, if succesful, you'll have a _key.pem, _cert.pem, .p12, .csr
}
alias createCA='_createCA'
