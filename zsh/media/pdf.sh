function _epub2pdf () {
  inFile=$1
  outFile=${inFile}.pdf
  pandoc -f epub -t latex ${inFile} -o ${outFile}
}
alias epub2pdf='_epub2pdf'

alias pdf2txt='pdftotext'

# https://askubuntu.com/a/256449
function _compressPDF () {
  inFile=$1
  settings=$2  # {screen, ebook, printer, prepress}

  outFile=${inFile}".compressed.pdf"
  gs -sDEVICE=pdfwrite -dCompatibilityLevel=1.4 -dPDFSETTINGS=/${settings} -dNOPAUSE -dQUIET -dBATCH -sOutputFile=${outFile} ${inFile}
}
alias compressPDF='_compressPDF'

function _decryptPDF () {
  inp=$1
  out="out.pdf"
  pass=$2

  pdftk ${inp} input_pw ${pass} output ${out}
  mv ${out} ${inp}  # overwrite
}
alias decryptPDF='_decryptPDF'

function searchPDFs () {
    inp=$1
    pdfgrep -n -i ${inp} *.pdf
}

function searchPDFFile () {
    inp=$1
    fpath=$2
    pdfgrep -n -i ${inp} ${fpath}
}

# https://askubuntu.com/a/256449
function shrinkSizePDF () {
    inp=$1
    out=shrinked_version_of_$1

    gs -sDEVICE=pdfwrite -dCompatibilityLevel=1.4 -dPDFSETTINGS=/ebook -dNOPAUSE -dQUIET -dBATCH -sOutputFile=${out} ${inp}
}
